package com.checkin.helpers

import android.Manifest
import androidx.multidex.BuildConfig
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

object AppConstants {

//   var BaseUrl = "https://localhost:8080/sharada-industries/";
 //  var BaseUrl = " http://192.168.1.6:8080/sharada-industries/";
var BaseUrl =  "http://192.168.0.104:8080/sharada-industries/"
var BaseUrl2 =  "https://ssi-admin-portal-nodejs.herokuapp.com/transactions/"



   const val THEMECOLOR = "#274493"
    const val DIVIDER_COLOR = "#e0e0e0"
    const val TRANSPARENT = "#00FFFFFF"

    const val COMMON_ID = "CommonID"
    const val TOOLBAR_TITLE = "ToolbarTitle"
    const val IS_SELECT_BANK = "IsSelectBank"
    const val LINK = "Link"
    const val STATES = "states"
    const val CITIES = "cities"
    const val RESEND_OTP = "otpResend"
    const val VALIDATE_OTP = "otpValidate"
    const val ITEM = "item"
    const val BASE64 = "base64"


    const val INVITE_CODE = "AJD9G5NJU"

    //  intent key
    const val OTP_RESPONSE = "otpResponse"
    const val VALIDATE_RESPONSE = "validateResponse"
    const val SCREEN_NAME = "screenName"
    const val BUNDLE = "bundel"
    const val MOBILE_NUMBER = "mobileNumber"
    const val DETAILS = "details"

    const val INTEREST_RATE_7_DAYS = 1.2f
    const val INTEREST_RATE_15_DAYS = 1.3f
    const val INTEREST_RATE_30_DAYS = 1.5f
    const val COMMISSION_RATE = 1f

    const val PROCESSING_RATE_7_DAYS = 14f
    const val PROCESSING_RATE_15_DAYS = 19f
    const val PROCESSING_RATE_30_DAYS = 22f

    const val GST_RATE = 18f

    /*expire validity*/
    const val VALIDITY_45 = 45
    const val VALIDITY_90 = 90
    const val VALIDITY_180 = 180
    const val VALIDITY_270 = 270
    const val VALIDITY_360 = 360


    const val REFER_FRIEND_EARN = 100
    const val REFER_YOU_EARN = 50
    const val COINS_50 = 50
    const val COINS_50_EQUALS = 10

    const val STATUS_PENDING = "pending"
    const val STATUS_PAID = "paid"
    const val STATUS_REJECTED = "rejected"

    const val STATUS_PENDING_COLOR = "#FCC007"
    const val STATUS_PAID_COLOR = "#00A657"
    const val STATUS_REJECTED_COLOR = "#A61A00"

    const val STATES_JSON = "indian_states_districts.json"
    const val INDUSTRY_TYPES_JSON = "industry_types.json"


    const val SOCKET_TIMEOUT = "java.net.SocketTimeoutException"
    const val INVALID_HOSTNAME = "java.net.UnknownHostException"
    const val CONNECTION_GONE = "java.net.ConnectException"
    const val NO_CONNECTION = "Unable to resolve host"


    /*this key also used for map , used in manifest file*/
    const val PLACE_API_KEY = "AIzaSyD6Zu4ZinMskW3kowCvT2DTpEws00TPW4o"

    const val SIMPLE_DATE_FORMAT = "dd-MM-yyyy"

    const val SIMPLE_DATE_FORMAT_MONTH = "MM-dd-yyyy"

    const val SIMPLE_DATE_FORMAT_YEAR = "yyyy-MM-dd"

    const val SIMPLE_DATE_FORMAT_WITH_TIME = "dd-MM-yyyy h:mm a"

    const val SIMPLE_DATE_FORMAT_DASH_FORMAT = "dd/MM/yyyy"

    const val SIMPLE_DATE_FORMAT_DAY_MONTH = "dd MMM"

    const val SIMPLE_DATE_DAY_MONTH_YEAR = "dd MMM yyyy"

    const val SIMPLE_DATE_FORMAT_DAY = "EEEE"

    const val SIMPLE_DATE_FORMAT_YEAR_FIRST_WITH_T_SSSS = "yyyy-MM-dd'T'HH:mm:ss.SS"

    const val SIMPLE_DATE_FORMAT_REQUEST_UTC = "yyyy-MM-dd'T'HH:mm:ss"

    const val SIMPLE_TIME_FORMAT_12 = "HH:mm a"

    const val SIMPLE_TIME_12 = "h:mm a"

    var decimalFormatOnePlace = NumberFormat.getNumberInstance(Locale("en", "US")) as DecimalFormat
    var decimalFormatTwoPlace = NumberFormat.getNumberInstance(Locale("en", "US")) as DecimalFormat

    var PERMISSIONS_CAMERA = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    var PERMISSIONS_CAMERA_VIDEO = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    var PERMISSIONS_RECEIVE_SMS =
        arrayOf(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS)

    var PERMISSIONS_READ_WRITE_STORAGE = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    var PERMISSIONS_LOCATION = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    const val sizeKB: Long = 1024
    const val sizeMB = sizeKB * sizeKB
    const val sizeGB = sizeMB * sizeKB
    const val sizeTB = sizeGB * sizeKB
    const val size2GBInMB = sizeKB * 2

    const val ORDER_ASC = "asc"
    const val ORDER_DESC = "desc"
    const val ALL_STORES = true
    var ACCESS_TOKEN = ""
    var APP_LANG = "en"
    var IS_APP_LANG_CHANGED = false

    const val EXTRA_OTP_TOKEN = "extra otp token"
    const val HEADER_KEY_AUTH = "x-auth-token"
    const val HEADER_FOR_YOU = "Token"
    const val HEADER_AUTHORIZATION = "Authorization"

    const val ARN_NOTIFICATION = "arn:aws:sns:us-east-1:238527534465:app/GCM/Painters"
    const val APP_LINK =
        "https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}"

    fun setLang(lang: String) {
        APP_LANG = lang
    }

    fun setAccessToken(token: String) {
        ACCESS_TOKEN = token
    }

    fun setIsLangChanged(isChaged: Boolean) {
        IS_APP_LANG_CHANGED = isChaged
    }
}
