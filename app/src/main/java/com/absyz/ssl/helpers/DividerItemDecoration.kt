package com.absyz.kotlin.helpers

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.absyz.ssl.R

class DividerItemDecoration : RecyclerView.ItemDecoration {

    protected var mOrientation: Int = 0
    protected var padding: Int = 0
    protected var startpadding: Int = 0
    protected var endpadding: Int = 0
    protected var dividerHeight: Int = 0
    protected var mContext: Context
    protected var mPaddingPaint: Paint? = null
    protected var mDividerPaint: Paint? = null

    constructor(context: Context) : this(context, VERTICAL_LIST, -1, 0) {}

    @JvmOverloads
    constructor(context: Context, orientation: Int, padding: Int = -1, dividerHeight: Int = -1) {
        setOrientation(orientation)
        mContext = context

        init()
        if (padding != -1) this.padding = padding
        updatePaddint()
        if (dividerHeight != -1) this.dividerHeight = dividerHeight
    }

    constructor(context: Context, orientation: Int, startpadding: Int, endpadding: Int, dividerHeight: Int) {

        setOrientation(orientation)
        mContext = context

        init()
        if (startpadding != -1) this.startpadding = startpadding
        if (endpadding != -1) this.endpadding = endpadding
        if (dividerHeight != -1) this.dividerHeight = dividerHeight
    }

    private fun updatePaddint() {
        startpadding = padding
        endpadding = padding
    }

    private fun init() {
        padding = mContext.resources.getDimensionPixelSize(R.dimen.margin_10)
        updatePaddint()
        dividerHeight = DEFAULT_DIVIDER_HEIGHT

        mPaddingPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        mPaddingPaint!!.color = ContextCompat.getColor(mContext, android.R.color.white)
        mPaddingPaint!!.style = Paint.Style.FILL

        mDividerPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        // mDividerPaint.setColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
        mDividerPaint!!.color = ContextCompat.getColor(mContext, R.color.color_grey_500)
        mDividerPaint!!.style = Paint.Style.FILL
    }

    fun setOrientation(orientation: Int) {
        if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
            throw IllegalArgumentException("invalid orientation")
        }
        mOrientation = orientation
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(c, parent, state)
        if (mOrientation == VERTICAL_LIST) {
            drawVertical(c, parent)
        } else {
            drawHorizontal(c, parent)
        }
    }

    fun drawVertical(c: Canvas, parent: RecyclerView) {
        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight

        val childCount = parent.childCount
        for (i in 0 until childCount - 1) {
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams
            val top = child.bottom + params.bottomMargin +
                    Math.round(ViewCompat.getTranslationY(child))
            val bottom = top + dividerHeight

            c.drawRect(
                left.toFloat(),
                top.toFloat(),
                (left + startpadding).toFloat(),
                bottom.toFloat(),
                mPaddingPaint!!
            )
            c.drawRect(
                (right - endpadding).toFloat(),
                top.toFloat(),
                right.toFloat(),
                bottom.toFloat(),
                mPaddingPaint!!
            )
            c.drawRect(
                (left + startpadding).toFloat(),
                top.toFloat(),
                (right - endpadding).toFloat(),
                bottom.toFloat(),
                mDividerPaint!!
            )
        }
    }

    fun drawHorizontal(c: Canvas, parent: RecyclerView) {
        val top = parent.paddingTop
        val bottom = parent.height - parent.paddingBottom

        val childCount = parent.childCount
        for (i in 0 until childCount - 1) {
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams
            val left = child.right + params.rightMargin +
                    Math.round(ViewCompat.getTranslationX(child))
            val right = left + dividerHeight
            c.drawRect(
                left.toFloat(),
                top.toFloat(),
                right.toFloat(),
                (top + startpadding).toFloat(),
                mPaddingPaint!!
            )
            c.drawRect(
                left.toFloat(),
                (bottom - endpadding).toFloat(),
                right.toFloat(),
                bottom.toFloat(),
                mPaddingPaint!!
            )
            c.drawRect(
                left.toFloat(),
                (top + startpadding).toFloat(),
                right.toFloat(),
                (bottom - endpadding).toFloat(),
                mDividerPaint!!
            )
        }
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        if (mOrientation == VERTICAL_LIST) {
            if (parent.getChildAdapterPosition(view) != parent.adapter!!.itemCount - 1) {
                outRect.set(0, 0, 0, dividerHeight)
            } else {
                outRect.set(0, 0, 0, 0)
            }
        } else {
            if (parent.getChildAdapterPosition(view) != parent.adapter!!.itemCount - 1) {
                outRect.set(0, 0, dividerHeight, 0)
            } else {
                outRect.set(0, 0, 0, 0)
            }
        }

    }

    companion object {
        private val DEFAULT_DIVIDER_HEIGHT = 0

        val HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL

        val VERTICAL_LIST = LinearLayoutManager.VERTICAL
    }
}
