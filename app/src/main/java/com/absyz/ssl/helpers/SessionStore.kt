package com.checkin.helpers

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import com.absyz.ssl.model.LoginResponseModel
import com.google.gson.Gson

class SessionStore(context: Context) {

    private val editor: Editor
    private val sharedPreferences: SharedPreferences

    companion object {

        const val sharePrefFileName = "kashVPref"

        const val USER_MODEL = "userModel"
        const val ADDRESS = "Address"
        const val USER_DETAILS = "userDetails"
        const val ACCESS_TOKEN = "AccessToken"
        const val VACCINE_CENTER = "VaccineCenter"
        const val CLIENT_DETAILS = "clientDetails"
        const val KYC = "kyc"
        const val EMPLOYMENT = "employment"
        const val USER_ID = "userID"
        const val MOBILE = "Mobile"
        const val COUNTRY_CODE = "CityCode"
        const val IS_LANGUAGE_SELECTED = "isLanguageSelected"
        const val IS_PERMISSION_GUIDE_SHOWN = "isPermissionGuide"

    }

    init {
        sharedPreferences = context.getSharedPreferences(sharePrefFileName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
    }

    fun saveString(context: Context, key: String, value: String) {
        editor.putString(key, value)
        editor.commit()
    }

    fun saveInt(context: Context, key: String, value: Int) {
        editor.putInt(key, value)
        editor.commit()
    }

    fun getString(key: String, defaultValue: String): String? {
        return sharedPreferences.getString(key, defaultValue)
    }

    fun getToken(): String? {
        return sharedPreferences.getString(ACCESS_TOKEN, "")
    }

    fun getTokenBearer(): String? {
        return "Bearer " + sharedPreferences.getString(ACCESS_TOKEN, "")
    }

    fun getInt(key: String, defaultValue: Int): Int {
        return sharedPreferences.getInt(key, defaultValue)
    }

    fun saveBoolean(key: String, value: Boolean) {
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, defaultValue)
    }

    /////checkin
    /*fun saveUserDetails(validateUserResponse: ValidateUserResponse?) {
        try {
            var json = ""
            validateUserResponse?.let {
                json = Gson().toJson(validateUserResponse)
            }
            editor.putString(USER_DETAILS, json)
            editor.putString(ACCESS_TOKEN, validateUserResponse!!.access_token)
            editor.putString(USER_ID, validateUserResponse!!.userID)
            editor.commit()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getUserDetails(): ValidateUserResponse? {
        var validateUserResponse: ValidateUserResponse? = null
        try {
            val json = sharedPreferences.getString(USER_DETAILS, "")
            validateUserResponse = Gson().fromJson(json, ValidateUserResponse::class.java)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return validateUserResponse
    }*/

    fun clearSharePref() {
        editor.clear()
        editor.commit()
    }

    /////checkin
    fun saveUserDetails(validateUserResponse: LoginResponseModel?) {
        try {
            var json = ""
            validateUserResponse?.let {
                json = Gson().toJson(validateUserResponse)
            }
            editor.putString(USER_DETAILS, json)
            editor.putString(ACCESS_TOKEN, validateUserResponse!!.token)
            editor.putString(USER_ID, validateUserResponse!!.userId)
            editor.commit()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getUserDetails(): LoginResponseModel? {
        var validateUserResponse: LoginResponseModel? = null
        try {
            val json = sharedPreferences.getString(USER_DETAILS, "")
            validateUserResponse = Gson().fromJson(json, LoginResponseModel::class.java)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return validateUserResponse
    }



}
