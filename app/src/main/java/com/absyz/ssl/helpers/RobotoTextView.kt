package com.checkin.helpers

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView


class RobotoTextView : AppCompatTextView {

    constructor(context: Context?) : super(context!!) {
        if (isInEditMode) return
        //parseAttributes(null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!, attrs
    ) {
        if (isInEditMode) return
        //parseAttributes(attrs)
    }

    /*private fun parseAttributes(attrs: AttributeSet?) {
        val typeface: Int
        try {
            if (attrs == null) { //Not created from xml
                typeface = Roboto.ROBOTO_REGULAR
            } else {
                val values = context.obtainStyledAttributes(attrs, R.styleable.FontFamily)
                typeface = values.getInt(R.styleable.FontFamily, Roboto.ROBOTO_REGULAR)
                values.recycle()
            }
            setTypeface(getRoboto(typeface))
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }*/

    private fun getRoboto(typeface: Int): Typeface? {
        return getRoboto(context, typeface)
    }

    object Roboto {
        const val ROBOTO_REGULAR = 0
        const val ROBOTO_MEDIUM = 1
        const val ROBOTO_BOLD = 2
        var sRobotoRegular: Typeface? = null
        var sRobotoMedium: Typeface? = null
        var sRobotoBold: Typeface? = null
    }

    companion object {

        const val FONT_ROBOTO_REGULAR = "fonts/helvetica.ttf"
        const val FONT_ROBOTO_BOLD = "fonts/helvetica-bold.ttf"

        fun getRoboto(context: Context, typef: Int): Typeface? {

            var typeface: Typeface?

            when (typef) {
                Roboto.ROBOTO_BOLD -> {
                    if (Roboto.sRobotoBold == null)
                        Roboto.sRobotoBold =
                            Typeface.createFromAsset(context.assets, FONT_ROBOTO_BOLD)
                    typeface = Roboto.sRobotoBold
                }
                Roboto.ROBOTO_MEDIUM -> {
                    if (Roboto.sRobotoMedium == null)
                        Roboto.sRobotoMedium =
                            Typeface.createFromAsset(context.assets, FONT_ROBOTO_BOLD)
                    typeface = Roboto.sRobotoMedium
                }
                Roboto.ROBOTO_REGULAR -> {
                    if (Roboto.sRobotoRegular == null) {
                        Roboto.sRobotoRegular =
                            Typeface.createFromAsset(context.assets, FONT_ROBOTO_REGULAR)
                    } else if (AppConstants.IS_APP_LANG_CHANGED) {
                        Roboto.sRobotoRegular =
                            Typeface.createFromAsset(context.assets, FONT_ROBOTO_REGULAR)
                        //SharedPreferencesHelper.isChangeLanguage(false, context)
                        AppConstants.setIsLangChanged(false)
                    }
                    typeface = Roboto.sRobotoRegular
                }
                else -> {
                    if (Roboto.sRobotoRegular == null) {
                        Roboto.sRobotoRegular =
                            Typeface.createFromAsset(context.assets, FONT_ROBOTO_REGULAR)
                    }
                    typeface = Roboto.sRobotoRegular
                }
            }

            return typeface;

        }
    }
}
