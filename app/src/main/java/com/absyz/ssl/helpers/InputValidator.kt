package com.checkin.helpers

import android.util.Patterns
import java.util.regex.Pattern

class InputValidator {

    fun isValidEmail(name: String?): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        val matcher = pattern.matcher(name)
        return matcher.matches()
    }

    fun isValidPassword(name: String?): Boolean {
        val patternString = "^(?=.*[0-9])(?=.*[a-zA-Z])(?=\\S+$).{8,15}$"
        val pattern = Pattern.compile(patternString)
        val matcher = pattern.matcher(name)
        return matcher.matches()
    }

    fun isValidName(name: String?): Boolean {
        val petternString = "^[a-zA-Z ]{3,50}$"
        val pattern = Pattern.compile(petternString)
        val matcher = pattern.matcher(name)
        return matcher.matches()
    }

    fun isValidAccountNumber(name: String?): Boolean {
        val petternString = "^[0-9]{9,18}$"
        val pattern = Pattern.compile(petternString)
        val matcher = pattern.matcher(name)
        return matcher.matches()
    }

    fun isValidCompanyName(name: String?): Boolean {
        val petternString = "^[a-zA-Z ]{8,100}$"
        val pattern = Pattern.compile(petternString)
        val matcher = pattern.matcher(name)
        return matcher.matches()
    }

    fun isValidPan(name: String?): Boolean {
        val petternString = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
        val pattern = Pattern.compile(petternString)
        val matcher = pattern.matcher(name)
        return matcher.matches()
    }

    fun isValidIFSC(name: String?): Boolean {
        val petternString = "^[A-Z]{4}0[A-Z0-9]{6}\$"
        val pattern = Pattern.compile(petternString)
        val matcher = pattern.matcher(name)
        return matcher.matches()
    }


    fun isValidPhoneNumber(strNum: String?, minPhoneDigits: Int, maxPhoneDigits: Int): Boolean {
        var isTrue = false
        val number: Long
        try {
            if (strNum == null) {
                isTrue = false
            } else {
                try {
                    number = strNum.toLong()
                    if (number.toString().length in minPhoneDigits..maxPhoneDigits)
                        isTrue = true
                } catch (nfe: NumberFormatException) {
                    isTrue = false
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return isTrue
    }


}