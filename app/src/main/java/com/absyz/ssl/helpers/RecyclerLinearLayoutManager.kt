package com.kashv.android.helpers

import android.content.Context
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler


class RecyclerLinearLayoutManager : LinearLayoutManager {
    constructor(
        context: Context?,
        @RecyclerView.Orientation orientation: Int,
        reverseLayout: Boolean
    ) : super(context) {
        try {
            Log.e(TAG, "RecyclerLinearLayoutManager()")
            setOrientation(orientation)
            setReverseLayout(reverseLayout)
        } catch (e: Exception) {
            Log.e(TAG, "Exception - " + e.message)
        }
    }

    constructor(context: Context?) : super(context) {
        try {
            Log.e(TAG, "RecyclerLinearLayoutManager()")
            orientation = VERTICAL
            reverseLayout = false
        } catch (e: Exception) {
            Log.e(TAG, "Exception - " + e.message)
        }
    }

    override fun onLayoutChildren(recycler: Recycler, state: RecyclerView.State) {
        try {
            super.onLayoutChildren(recycler, state)
        } catch (e: IndexOutOfBoundsException) {
            Log.e(TAG, "IndexOutOfBoundsException - " + e.message)
        }
    }

    companion object {
        private const val TAG = "RecycLinLayManager"
    }
}