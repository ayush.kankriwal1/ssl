package com.checkin.dagger.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.absyz.ssl.dagger.factory.ViewModelFactory
import com.absyz.ssl.viewmodel.HomeViewModel
import com.absyz.ssl.viewmodel.SignUpViewModel
import com.checkin.dagger.scope.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {


   @Binds
   abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

//    @Binds
//    @IntoMap
//    @ViewModelKey(ViewModel::class)
//    abstract fun bindCountryViewModel(repoViewModel: ViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignUpViewModel::class)
    abstract fun bindSignUpViewModel(repoViewModel: SignUpViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(repohomeViewModel: HomeViewModel): ViewModel

}
