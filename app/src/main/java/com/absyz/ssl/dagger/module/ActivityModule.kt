package com.checkin.dagger.module

import android.content.Context
import android.content.res.AssetManager

import androidx.appcompat.app.AppCompatActivity

import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * Created by Ayush on 30-05-2021.
 */

@Module
class ActivityModule(private val mActivity: AppCompatActivity) {

    @Provides
     fun provideContext(): Context {
        return mActivity
    }

    @Provides
     fun provideActivity(): AppCompatActivity {
        return mActivity
    }

    @Provides
     fun provideCompositeSubscription(): CompositeSubscription {
        return CompositeSubscription()
    }

    @Provides
     fun provideAssetManager(): AssetManager {
        return mActivity.assets
    }

    /*@Provides
    CountriesAdapter provideCountriesAdapter() {
        return new CountriesAdapter();
    }*/


}
