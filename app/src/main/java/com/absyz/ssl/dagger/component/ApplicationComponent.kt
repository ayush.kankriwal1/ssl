package com.checkin.dagger.component

import android.content.Context
import com.absyz.ssl.activities.*
import com.absyz.ssl.photoview.ImageCropActivity

import com.checkin.helpers.SessionStore
import com.checkin.helpers.Utility
import com.checkin.dagger.module.ApplicationModule
import com.checkin.dagger.module.NetworkModule
import dagger.Component
import fragment.BaseFragment
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetworkModule::class])
interface ApplicationComponent {

    fun context(): Context

    fun sessionStore(): SessionStore // can inject util/object in another helper class by using this

    fun utility(): Utility // can inject util/object in another helper class

    fun inject(fragment: BaseFragment)
    fun inject(activity: SessionStore)
    fun inject(activity: ImageCropActivity)
    fun inject(activity: AssignWorkActivity)
    fun inject(activity: EditVendorActivity)
    fun inject(activity: EditPeopleActivity)
    fun inject(activity: GroupsActivity)
    fun inject(activity: TransactionActivity)

}

