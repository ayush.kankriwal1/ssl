package com.checkin.dagger.component

import com.checkin.dagger.module.ActivityModule

import javax.inject.Singleton

import dagger.Component

@Singleton
@Component(modules = [ActivityModule::class])
interface ActivityComponent {

/*The injector class used in Dagger 2 is called a component.
     It assigns references in our activities, services, or fragments to have access to singletons we earlier defined.
      We will need to annotate this class with a @Component annotation*//*Note that the activities, services, or fragments that are allowed to request the dependencies
    declared by the modules (by means of the @Inject annotation) should be declared in this class
    with individual inject() methods*/

//SessionStore sessionStore();// can inject util/object in another helper class by using this
//Utility utility();// can inject util/object in another helper class
//ViewModelFactory viewModelFactory();
//void inject(MainActivity activity);
//void inject(CountriesActivity activity);
// void inject(MyFragment fragment);
// void inject(MyService service);

}