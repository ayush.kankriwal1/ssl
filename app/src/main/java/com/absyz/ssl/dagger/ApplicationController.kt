package com.absyz.kotlin.dagger

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Environment
import android.os.StatFs
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.BuildConfig
import androidx.multidex.MultiDexApplication
import com.absyz.ssl.R
import com.bumptech.glide.request.target.ViewTarget
import com.checkin.dagger.component.ApplicationComponent
import com.checkin.dagger.module.ApplicationModule
import com.checkin.dagger.module.NetworkModule
import com.checkin.helpers.AppConstants
import com.checkin.dagger.component.DaggerApplicationComponent

class ApplicationController : MultiDexApplication() {

    lateinit var applicationComponent: ApplicationComponent

    var context: Context? = null


    override fun onCreate() {
        super.onCreate()
        try {

            applicationComponent = DaggerApplicationComponent.builder()
                // list of modules that are part of this component need to be created here too
                .networkModule(NetworkModule(AppConstants.BaseUrl))
                .applicationModule(ApplicationModule(this)) // This also corresponds to the name of your module: %component_name%Module // This also corresponds to the name of your module: %component_name%Module
                .build()

            //FacebookSdk.sdkInitialize(applicationContext)

            ViewTarget.setTagId(R.id.glide_tag);

            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

//            setUpCrashlytics()

            //MapsInitializer.initialize(this)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    /* public ActivityComponent getActivityComponent() {
        return mActivityComponent;
    }*/

    override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
    }

    companion object {

        const val TAG = "ApplicationController"
        private const val FEATURE_APP_USES_KOTLIN = "KT"

        fun getApp(activity: Activity): ApplicationController {
            return activity.application as ApplicationController
        }
    }

    override fun onTerminate() {
        Log.e(TAG, "onTerminate()")
        super.onTerminate()
    }

//    private fun setUpCrashlytics() {
////        try {
////
////            FirebaseCrashlytics.getInstance()
////                .setCrashlyticsCollectionEnabled(BuildConfig.IS_CRASHLYTICS_ENABLE)
////            //FirebaseAnalytics.getInstance(applicationContext).setAnalyticsCollectionEnabled(BuildConfig.IS_CRASHLYTICS)
////
////            val user = UserModel()
////            var crashlytics: FirebaseCrashlytics = FirebaseCrashlytics.getInstance()
////
////            Log.e(TAG, "setUpCrashlytics()")
////
////            if (user != null) {
////                crashlytics.setCustomKey("UserId", user.id)
////                crashlytics.setCustomKey("UserName", user.firstName)
////                //crashlytics.setCustomKey("UserEmail", user.email)
////            } else {
////                crashlytics.setCustomKey("UserId", "-1")
////                crashlytics.setCustomKey("UserName", "Guest")
////                crashlytics.setCustomKey("UserEmail", "guest@gmail.com")
////            }
////
////            //crashlytics.setCustomKey("Language", SharedPreferencesHelper.getLanguage(applicationContext))
////            crashlytics.setCustomKey("Android VERSION", android.os.Build.VERSION.RELEASE)
////            crashlytics.setCustomKey("Android DEVICE", android.os.Build.DEVICE)
////            crashlytics.setCustomKey("Android MODEL", android.os.Build.MODEL)
////            crashlytics.setCustomKey("Android BRAND", android.os.Build.BRAND)
////            crashlytics.setCustomKey("Android TYPE", android.os.Build.TYPE)
////            crashlytics.setCustomKey("App VERSION", BuildConfig.VERSION_NAME)
////
////
////            val stat = getStatFs()
////            crashlytics.setCustomKey("Total Internal Memory",
////                getTotalInternalMemorySize(stat)
////            )
////            crashlytics.setCustomKey(
////                "Available Internal Memory",
////                getAvailableInternalMemorySize(stat)
////            )
////
////
////            Log.e(TAG, "setUpCrashlytics() - end")
////
////        } catch (ex: java.lang.Exception) {
//            ex.printStackTrace()
//            Log.e(TAG, "setUpCrashlytics() -" + ex.message)
//            try {
////                FirebaseCrashlytics.getInstance()
////                    .setCustomKey(TAG, "setUpCrashlytics() Exception - " + ex.message)
//            } catch (ex: java.lang.Exception) {
//                ex.printStackTrace()
//            }
//
//        }


    }


    private fun getStatFs(): StatFs {
        val path = Environment.getDataDirectory()
        return StatFs(path.path)
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private fun getAvailableInternalMemorySize(stat: StatFs): String {
        val blockSize = stat.blockSizeLong
        val availableBlocks = stat.availableBlocksLong
        return formatSize(availableBlocks * blockSize)
    }

    private fun getTotalInternalMemorySize(stat: StatFs): String {
        val blockSize = stat.blockSizeLong
        val totalBlocks = stat.blockCountLong
        return formatSize(totalBlocks * blockSize)
    }

    fun formatSize(size: Long): String {
        var size = size
        var suffix: String? = null
        var resultBuffer: StringBuilder? = StringBuilder("")
        try {
            if (size >= 1024) {
                suffix = "KB"
                size /= 1024
                if (size >= 1024) {
                    suffix = "MB"
                    size /= 1024

                    if (size >= 1024) {
                        suffix = "GB"
                        size /= 1024
                    }
                }
            }

            resultBuffer = StringBuilder(size.toString())

            var commaOffset = resultBuffer.length - 3
            while (commaOffset > 0) {
                resultBuffer.insert(commaOffset, ',')
                commaOffset -= 3
            }

            if (suffix != null) resultBuffer.append(suffix)

        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }

        return resultBuffer.toString()
    }

