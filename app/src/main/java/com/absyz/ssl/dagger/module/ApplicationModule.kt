package com.checkin.dagger.module

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.checkin.helpers.InputValidator
import com.checkin.helpers.SessionStore
import com.checkin.helpers.Utility
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class ApplicationModule(var mApplication: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return mApplication.applicationContext
    }

    @Provides
    @Singleton
    fun provideAppCompatActivity(context: Context): AppCompatActivity {
        return context as AppCompatActivity
    }

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return mApplication
    }

    @Provides
    @Singleton
    fun provideSessionStore(context: Context): SessionStore {
        return SessionStore(context)
    }

    @Provides
    @Singleton
    fun provideUtility(context: Context): Utility {
        return Utility(context)
    }

    @Provides
    @Singleton
    fun provideInputValidator(): InputValidator {
        return InputValidator()
    }

   /* @Provides
    @Singleton
    fun providePickMediaActivity(context: Context): PickMediaActivity {
        return PickMediaActivity(context)
    }*/

    /*@Provides
    internal fun provideCountriesAdapter(): CountriesAdapter {
        return CountriesAdapter
    }*/


}
