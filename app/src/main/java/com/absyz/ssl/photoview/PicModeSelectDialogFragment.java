package com.absyz.ssl.photoview;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.absyz.ssl.cameraview.UtilityJava;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class PicModeSelectDialogFragment extends DialogFragment {

    private String[] picMode = {UtilityJava.PicModes.CAMERA,UtilityJava.PicModes.GALLERY};

    private IPicModeSelectListener iPicModeSelectListener;

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setTitle("Select Mode")
                .setItems(picMode, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(iPicModeSelectListener != null) iPicModeSelectListener.onPicModeSelected(picMode[which]);
                    }
                });
        return builder.create();
    }

    public void setiPicModeSelectListener(IPicModeSelectListener iPicModeSelectListener) {
        this.iPicModeSelectListener = iPicModeSelectListener;
    }



    public interface IPicModeSelectListener{
        void onPicModeSelected(String mode);
    }
}
