package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName

data class UnassignResponseModel(
    @SerializedName("message"    ) var message    : String?           = null,
    @SerializedName("result"     ) var result     : ArrayList<String> = arrayListOf(),
    @SerializedName("statusCode" ) var statusCode : String?           = null
    )