package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName

data class TransactionResponseModel (
    @SerializedName("message" ) var message : String?           = null,
    @SerializedName("status"  ) var status  : Int?              = null,
    @SerializedName("result"  ) var result  : ArrayList<String> = arrayListOf()

)