package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName

data class TransactionRequestModel(
    @SerializedName("transactionid" ) var transactionid : String? = null,
    @SerializedName("quantity"      ) var quantity      : String? = null,
    @SerializedName("vendorid"      ) var vendorid      : String? = null,
    @SerializedName("volunteer_id"  ) var volunteerId   : String? = null,
    @SerializedName("amount"        ) var amount        : String? = null
)
