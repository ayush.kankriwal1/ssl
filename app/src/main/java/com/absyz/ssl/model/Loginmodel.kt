package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Loginmodel(
    @SerializedName("password") var password: String? = null,
    @SerializedName("username") var username: String? = null
)

data class LoginResponseModel(
    @SerializedName("userId") var userId: String? = null,
    @SerializedName("token") var token: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("message") var message: Message? = Message()

) : Serializable

data class Message(

    @SerializedName("id") var id: Int? = null,
    @SerializedName("first_name") var firstName: String? = null,
    @SerializedName("last_name") var lastName: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("password") var password: String? = null,
    @SerializedName("phone") var phone: String? = null,
    @SerializedName("group_id") var groupId: Int? = null,
    @SerializedName("join_date") var joinDate: String? = null,
    @SerializedName("parent_id") var parentId: Int? = null,
    @SerializedName("employer_id") var employerId: Int? = null,
    @SerializedName("role_id") var roleId: Int? = null,
    @SerializedName("code") var code: String? = null,
    @SerializedName("profile_pic") var profilePic: String? = null,
    @SerializedName("status") var status: Int? = null,
    @SerializedName("create_date") var createDate: ArrayList<Int> = arrayListOf(),
    @SerializedName("modified_date") var modifiedDate: ArrayList<Int> = arrayListOf()

) : Serializable