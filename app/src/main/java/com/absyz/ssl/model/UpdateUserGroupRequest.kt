package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName

data class UpdateUserGroupRequest (
    @SerializedName("create_date" ) var createDate : String? = null,
    @SerializedName("group_name"  ) var groupName  : String? = null,
    @SerializedName("id"          ) var id         : Int?    = null
    )