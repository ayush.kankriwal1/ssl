package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import kotlin.Result


data class HomeViewModelResponse(
    @SerializedName("message") val message: String? = null,
    @SerializedName("result") val result: List<Resultdata> = arrayListOf(),
    @SerializedName("status") val status: String? = null

) : Serializable


data class Resultdata(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("group_name") val group_name: String? = null,
    @SerializedName("create_date") val create_date: Double? = null
) : Serializable


data class ProductsAssigned(
    @SerializedName("message") var message: String? = null,
    @SerializedName("result") var result: ArrayList<ResultProduct> = arrayListOf(),
    @SerializedName("statusCode") var statusCode: String? = null
) : Serializable

data class ResultProduct(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("product_id") var productId: Int? = null,
    @SerializedName("price") var price: Int? = null,
    @SerializedName("user_id") var userId: Int? = null,
    @SerializedName("create_date") var createDate: String? = null,
    @SerializedName("create_time") var createTime: String? = null,
    @SerializedName("delivery_date") var deliveryDate: String? = null,
    @SerializedName("status") var status: Int? = null,
    @SerializedName("size") var size: String? = null,
    @SerializedName("color") var color: String? = null,
    @SerializedName("quantity") var quantity: Int? = null,
    @SerializedName("product_title") var productTitle: String? = null,
    @SerializedName("total_price") var totalPrice: Int? = null
) : Serializable
