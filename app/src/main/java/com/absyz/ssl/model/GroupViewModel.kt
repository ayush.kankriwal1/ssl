package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class GroupOfPeopleModel(
    @SerializedName("message") var message: String? = null,
    @SerializedName("result") var result: ArrayList<ResultGroup> = arrayListOf(),
    @SerializedName("status") var status: String? = null
) : Serializable

data class ResultGroup(

    @SerializedName("id") var id: Int? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("adhar_no") var adharNo: String? = null,
    @SerializedName("created_date") var createdDate: ArrayList<Int> = arrayListOf(),
    @SerializedName("updated_date") var updatedDate: ArrayList<Int> = arrayListOf(),
    @SerializedName("status") var status: Int? = null,
    @SerializedName("group_id") var groupId: Int? = null,
    @SerializedName("profile_pic") var profilePic: String? = null,
    @SerializedName("proof_pic") var proofPic: String? = null
) : Serializable


data class Addpeopleingroup(
    @SerializedName("message") var message: String? = null,
    @SerializedName("result") var result: ResultAddpeople? = ResultAddpeople(),
    @SerializedName("status") var status: String? = null
) : Serializable

data class ResultAddpeople(

    @SerializedName("id") var id: Int? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("adhar_no") var adharNo: String? = null,
    @SerializedName("created_date") var createdDate: ArrayList<Int> = arrayListOf(),
    @SerializedName("updated_date") var updatedDate: ArrayList<Int> = arrayListOf(),
    @SerializedName("status") var status: Int? = null,
    @SerializedName("group_id") var groupId: Int? = null,
    @SerializedName("profile_pic") var profilePic: String? = null,
    @SerializedName("proof_pic") var proofPic: String? = null
) : Serializable


data class UploadResponse(

    @SerializedName("message") var message: String? = null,
    @SerializedName("result") var result: ResultUpload? = ResultUpload(),
    @SerializedName("status") var status: String? = null

) : Serializable

data class ResultUpload(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("image_name") var imageName: String? = null,
    @SerializedName("image_path") var imagePath: String? = null
) : Serializable