package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName

data class UpdatePeopleResponse (
    @SerializedName("message" ) var message : String? = null,
    @SerializedName("result"  ) var result  : Result? = Result(),
    @SerializedName("status"  ) var status  : String? = null
        )

//data class Result (
//
//    @SerializedName("id"           ) var id          : Int?           = null,
//    @SerializedName("name"         ) var name        : String?        = null,
//    @SerializedName("adhar_no"     ) var adharNo     : String?        = null,
//    @SerializedName("created_date" ) var createdDate : ArrayList<Int> = arrayListOf(),
//    @SerializedName("updated_date" ) var updatedDate : ArrayList<Int> = arrayListOf(),
//    @SerializedName("status"       ) var status      : Int?           = null,
//    @SerializedName("group_id"     ) var groupId     : Int?           = null,
//    @SerializedName("profile_pic"  ) var profilePic  : String?        = null,
//    @SerializedName("proof_pic"    ) var proofPic    : String?        = null
//)