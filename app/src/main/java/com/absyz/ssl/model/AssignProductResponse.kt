package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class AssignProductResponse(
    @SerializedName("message"    ) var message    : String?           = null,
    @SerializedName("statusCode" ) var statusCode : String?           = null,
    @SerializedName("result"     ) var result     : ArrayList<String> = arrayListOf()
) : Serializable
