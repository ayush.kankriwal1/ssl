package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName

data class UpdateUserGroupResponse (
    @SerializedName("message" ) var message : String? = null,
    @SerializedName("result"  ) var result  : Result? = Result(),
    @SerializedName("status"  ) var status  : String? = null
        )

data class Result (

    @SerializedName("id"          ) var id         : Int?    = null,
    @SerializedName("group_name"  ) var groupName  : String? = null,
    @SerializedName("create_date" ) var createDate : Int?    = null

)