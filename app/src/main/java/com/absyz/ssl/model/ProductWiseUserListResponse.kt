package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName

data class ProductWiseUserListResponse (
    @SerializedName("message"    ) var message    : String?           = null,
    @SerializedName("result"     ) var result     : ArrayList<ProductWiseResult> = arrayListOf(),
    @SerializedName("statusCode" ) var statusCode : String?           = null
        )

data class ProductWiseResult (

    @SerializedName("id"             ) var id            : Int?    = null,
    @SerializedName("transaction_id" ) var transactionId : Int?    = null,
    @SerializedName("group_id"       ) var groupId       : Int?    = null,
    @SerializedName("vendor_id"      ) var vendorId      : Int?    = null,
    @SerializedName("volunteer_id"   ) var volunteerId   : Int?    = null,
    @SerializedName("created_date"   ) var createdDate   : String? = null,
    @SerializedName("modified_date"  ) var modifiedDate  : String? = null,
    @SerializedName("product_title"  ) var productTitle  : String? = null,
    @SerializedName("manufacturer"   ) var manufacturer  : String? = null,
    @SerializedName("price"          ) var price         : Int?    = null,
    @SerializedName("name"           ) var name          : String? = null,
    @SerializedName("adhar_no"       ) var adharNo       : String? = null

)