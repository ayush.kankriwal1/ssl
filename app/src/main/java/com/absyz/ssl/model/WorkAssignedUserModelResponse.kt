package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName

data class WorkAssignedUserModelResponse (

    @SerializedName("message"    ) var message    : String?           = null,
    @SerializedName("result"     ) var result     : ArrayList<WorkAssignedUserResult> = arrayListOf(),
    @SerializedName("statusCode" ) var statusCode : String?           = null

)