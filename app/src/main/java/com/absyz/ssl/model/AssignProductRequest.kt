package com.absyz.ssl.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class AssignProductRequest(
    @SerializedName("transaction_id" ) var transactionId : String? ,
    @SerializedName("group_id"       ) var groupId       : String? = null,
    @SerializedName("userid"         ) var userid        : String? = null
) : Serializable
