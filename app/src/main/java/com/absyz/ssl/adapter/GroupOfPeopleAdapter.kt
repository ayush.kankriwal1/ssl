package com.absyz.ssl.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.ssl.R
import com.absyz.ssl.databinding.ItemGroupofpeopleAdapterBinding
import com.absyz.ssl.model.ResultGroup
import com.checkin.helpers.Utility
import java.text.SimpleDateFormat

class GroupOfPeopleAdapter(

    val context: Context,
    private val itemClickListener: ItemClickListener,
    val utility: Utility
) :
    RecyclerView.Adapter<GroupOfPeopleAdapter.ViewHolder>() {

    private var itemArrayList: List<ResultGroup>? = null
    val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy")

    companion object {
        const val TAG = "SearchAdapter"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemGroupofpeopleAdapterBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_groupofpeople_adapter,
            parent,
            false
        )
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            Log.e(TAG, "onBindViewHolder()")
            val listSelectionMode = itemArrayList!![position]
            holder.binding.groupName.text = listSelectionMode.name
            holder.binding.groupID.text = listSelectionMode.id.toString()
            holder.binding.adhaarID.text = listSelectionMode.adharNo
            holder.binding.status.text = listSelectionMode.status.toString()

//            holder.binding.tvSlotTime.text = ""

            /* utility!!.loadGlideWithCircleSkipCache(
                 context,
                 holder.binding.userImage,
                 "",Hom
                 R.drawable.profile_default
             )*/

            holder.binding.editIcon.setOnClickListener {
                itemClickListener.onItemClick(listSelectionMode)
            }


//            searchModel.slotTime?.let {
//                holder.binding.tvSlotTime.text = it.replace("T", ",")
//            }
            /*holder.binding.tvSlotTime.text = Utility.getDesiredDateFormatFromUTC(
                AppConstants.SIMPLE_DATE_FORMAT_REQUEST_UTC,
                AppConstants.SIMPLE_DATE_FORMAT_WITH_TIME,
                searchModel.slotTime
            )*/

        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "onBindViewHolder() Exception - ${ex.message}")
        }

    }

    override fun getItemCount(): Int {
        return if (itemArrayList != null) itemArrayList!!.size else 0
    }


    inner class ViewHolder(internal var binding: ItemGroupofpeopleAdapterBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun updateList(newList: List<ResultGroup>) {
        try {
            itemArrayList = newList
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "updateList() Exception - ${ex.message}")
        }

    }

    interface ItemClickListener {
        fun onItemClick(item: ResultGroup)
    }

}