package com.absyz.ssl.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import fragment.AllGroupofPeople
import fragment.AssignedGroupofPeople

class GroupTabPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    lateinit var grpID : String

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                AllGroupofPeople(grpID)
            }
            else ->
            {
                 AssignedGroupofPeople(grpID)
            }

        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "All People"
            else -> {
                return "Assigned People"
            }
        }
    }

    fun getgroupId(grpId : String) {
        try {
            grpID = grpId
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }
}