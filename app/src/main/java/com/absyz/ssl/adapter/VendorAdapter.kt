package com.absyz.ssl.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.ssl.R
import com.absyz.ssl.databinding.ItemProductAdapterBinding
import com.absyz.ssl.databinding.ItemVendoreAdapterBinding
import com.absyz.ssl.model.HomeViewModelResponse
import com.absyz.ssl.model.Resultdata
import com.checkin.helpers.Utility
import fragment.HomeFragment
import java.text.SimpleDateFormat

class VendorAdapter(
    val context: Context,
    val utility: Utility
) :
    RecyclerView.Adapter<VendorAdapter.ViewHolder>() {

    private var itemArrayList: List<Resultdata>? = null
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
    private lateinit var mListerner: VendorAdapter.onItemClickListener

    companion object {
        const val TAG = "SearchAdapter"
    }

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemVendoreAdapterBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_vendore_adapter,
            parent,
            false
        )
        return ViewHolder(binding,mListerner)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            Log.e(TAG, "onBindViewHolder()")
            val listSelectionMode = itemArrayList!![position]
            holder.binding.groupName.text =listSelectionMode.group_name
            holder.binding.groupID.text = listSelectionMode.id.toString()

            val dateString = simpleDateFormat.format(listSelectionMode.create_date)
            holder.binding.date.text  = dateString

//            holder.binding.editIcon.setOnClickListener {
//                itemClickListener.onItemClick(listSelectionMode)
//            }

//            holder.binding.tvSlotTime.text = ""

            /* utility!!.loadGlideWithCircleSkipCache(
                 context,
                 holder.binding.userImage,
                 "",Hom
                 R.drawable.profile_default
             )*/

//            holder.binding.cardView.setOnClickListener {
//                itemClickListener.onItemClick(searchModel)
//            }


//            searchModel.slotTime?.let {
//                holder.binding.tvSlotTime.text = it.replace("T", ",")
//            }
            /*holder.binding.tvSlotTime.text = Utility.getDesiredDateFormatFromUTC(
                AppConstants.SIMPLE_DATE_FORMAT_REQUEST_UTC,
                AppConstants.SIMPLE_DATE_FORMAT_WITH_TIME,
                searchModel.slotTime
            )*/

        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "onBindViewHolder() Exception - ${ex.message}")
        }

    }

    override fun getItemCount(): Int {
        return if (itemArrayList != null) itemArrayList!!.size else 0
    }


//    inner class ViewHolder(internal var binding: ItemVendoreAdapterBinding) :
//        RecyclerView.ViewHolder(binding.root)

    fun updateList(newList: List<Resultdata>) {
        try {
            itemArrayList = newList
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "updateList() Exception - ${ex.message}")
        }

    }


    inner class ViewHolder(internal var binding: ItemVendoreAdapterBinding, listner: VendorAdapter.onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }
        }
    }

}