package com.absyz.ssl.adapter

import android.content.Context
import android.opengl.Visibility
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.ssl.R
import com.absyz.ssl.databinding.ItemProductAdapterBinding
import com.absyz.ssl.model.ResultProduct
import com.absyz.ssl.model.Resultdata
import com.checkin.helpers.Utility
import fragment.HomeFragment

class ProductListAdapter (val context: Context, private val itemClickListener: ItemClickListener, val utility: Utility) :
    RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {

    private var itemArrayList: List<ResultProduct>? = null
    private lateinit var mListerner: onItemClickListener

    companion object {
        const val TAG = "SearchAdapter"
    }

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemProductAdapterBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_product_adapter,
            parent,
            false
        )
        return ViewHolder(binding,mListerner)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            Log.e(TAG, "onBindViewHolder()")
            val listSelectionMode = itemArrayList!![position]
            holder.binding.prodName.text =listSelectionMode.productTitle
            //holder.binding.groupID.text = listSelectionMode.productId.toString()
            //holder.binding.price.text = VISIBLE.toString()
            holder.binding.price.text = listSelectionMode.price.toString()
            holder.binding.deliverydate.text = listSelectionMode.deliveryDate
            holder.binding.qty.text = listSelectionMode.quantity.toString()

//            holder.binding.tvSlotTime.text = ""

            /* utility!!.loadGlideWithCircleSkipCache(
                 context,
                 holder.binding.userImage,
                 "",Hom
                 R.drawable.profile_default
             )*/
            holder.binding.transview.setOnClickListener {
                itemClickListener.onItemClick(listSelectionMode)
            }


//            searchModel.slotTime?.let {
//                holder.binding.tvSlotTime.text = it.replace("T", ",")
//            }
            /*holder.binding.tvSlotTime.text = Utility.getDesiredDateFormatFromUTC(
                AppConstants.SIMPLE_DATE_FORMAT_REQUEST_UTC,
                AppConstants.SIMPLE_DATE_FORMAT_WITH_TIME,
                searchModel.slotTime
            )*/

        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "onBindViewHolder() Exception - ${ex.message}")
        }

    }

    override fun getItemCount(): Int {
        return if (itemArrayList != null) itemArrayList!!.size else 0
    }


//    inner class ViewHolder(internal var binding: ItemProductAdapterBinding) :
//        RecyclerView.ViewHolder(binding.root)

    fun updateList(newList: List<ResultProduct>) {
        try {
            itemArrayList = newList
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "updateList() Exception - ${ex.message}")
        }

    }

    interface ItemClickListener {
        fun onItemClick(item: ResultProduct?)
    }

    inner class ViewHolder(internal var binding: ItemProductAdapterBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }
        }
    }

}