package com.absyz.ssl.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.absyz.ssl.model.LoginResponseModel
import com.absyz.ssl.model.Loginmodel
import com.checkin.network.ApiResponse
import com.checkin.network.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SignUpViewModel  @Inject constructor(
    private val repository: Repository,
    val context: Context
) : ViewModel() {
    private val disposables = CompositeDisposable()
    private val responseLiveData = MutableLiveData<ApiResponse<*>>()

    companion object {
        const val TAG = "SignUpViewModel"
    }

    fun response(): MutableLiveData<ApiResponse<*>> {
        return responseLiveData
    }

    fun validateUser(validateRequest: Loginmodel) {
        Log.e(TAG, "validateUser()")
        try {
            repository.validateUser(validateRequest)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { responseLiveData.setValue(ApiResponse.loading<LoginResponseModel>()) }
                ?.let {
                    disposables.add(
                        it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                responseLiveData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                responseLiveData.setValue(
                                    ApiResponse.error<LoginResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }

    }

}