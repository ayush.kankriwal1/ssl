package com.absyz.ssl.viewmodel


import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.absyz.ssl.model.*
import com.checkin.helpers.Utility
import com.checkin.network.ApiResponse
import com.checkin.network.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import javax.inject.Inject

class  HomeViewModel @Inject constructor(
    private val repository: Repository,
    val context: Context
) : ViewModel() {
    private val disposables = CompositeDisposable()
    private val responseLiveDatahome = MutableLiveData<ApiResponse<*>>()

    companion object {
        const val TAG = "HomeVieModel"
    }

    fun response(): MutableLiveData<ApiResponse<*>> {
        return responseLiveDatahome
    }
    val utility : Utility? = null

    private val responseLiveDatahomeProduct = MutableLiveData<ApiResponse<*>>()
    private val responseLiveDatauploadImage = MutableLiveData<ApiResponse<*>>()

    fun getuserdetailsResponse(): MutableLiveData<ApiResponse<*>>? {
        return responseLiveDatahomeProduct
    }

    fun vendorelistrequest(userId: Int?, s: String) {
        Log.e(HomeViewModel.TAG,  s)
        try {
            repository.requestvendore(userId, s, "application/json")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { responseLiveDatahome.setValue(ApiResponse.loading<HomeViewModelResponse>()) }
                ?.let {
                    disposables.add(
                        it
                            .subscribe(
                                { result ->
                                    Log.e(HomeViewModel.TAG, "responseDatahome()")
                                    Log.e("responseDatahome()", result.toString())
                                    responseLiveDatahome.setValue(ApiResponse.success(result))
                                },
                                { throwable ->
                                    //val responseCode = (throwable as HttpException).code()
                                    //Log.e(TAG, "responseCode() - $responseCode")
                                    responseLiveDatahome.setValue(
                                        ApiResponse.error<HomeViewModelResponse>(
                                            throwable
                                        )
                                    )
                                }
                            ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "HomeView()-Exception-" + ex.message)
        }

    }

    fun productlistrequest(userId: Int?, s: String) {
        Log.e(HomeViewModel.TAG,  s)
        try {
            repository.productlistrequest(userId, s, "application/json")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { responseLiveDatahomeProduct.setValue(ApiResponse.loading<ProductsAssigned>()) }
                ?.let {
                    disposables.add(
                        it
                            .subscribe(
                                { result ->
                                    Log.e(HomeViewModel.TAG, "ProductsAssigned()")
                                    Log.e("ProductsAssigned()", result.toString())
                                    responseLiveDatahomeProduct.setValue(ApiResponse.success(result))
                                },
                                { throwable ->
                                    //val responseCode = (throwable as HttpException).code()
                                    //Log.e(TAG, "responseCode() - $responseCode")
                                    responseLiveDatahomeProduct.setValue(
                                        ApiResponse.error<ProductsAssigned>(
                                            throwable
                                        )
                                    )
                                }
                            ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "responseLiveProductsAssigned()-Exception-" + ex.message)
        }

    }

    fun grouppeopleistrequest(userId: String?, s: String) {
        Log.e(HomeViewModel.TAG,  s)
        try {
            repository.grouplistrequest(userId, s, "application/json")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { responseLiveDatahome.setValue(ApiResponse.loading<GroupOfPeopleModel>()) }
                ?.let {
                    disposables.add(
                        it
                            .subscribe(
                                { result ->
                                    Log.e(HomeViewModel.TAG, "ProductsAssigned()")
                                    Log.e("ProductsAssigned()", result.toString())
                                    responseLiveDatahome.setValue(ApiResponse.success(result))
                                },
                                { throwable ->
                                    //val responseCode = (throwable as HttpException).code()
                                    //Log.e(TAG, "responseCode() - $responseCode")
                                    responseLiveDatahome.setValue(
                                        ApiResponse.error<GroupOfPeopleModel>(
                                            throwable
                                        )
                                    )
                                }
                            ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "responseLiveProductsAssigned()-Exception-" + ex.message)
        }

    }

    fun validateaddpeople(adharnumber: String, groupID: String, volunName : String, profPic :String, proofPic : String, token: String) {
        Log.e(HomeViewModel.TAG,  adharnumber)
        try {
            repository.addpeoplelistrequest(adharnumber, groupID, volunName,profPic, proofPic, "application/json", token)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { responseLiveDatahome.setValue(ApiResponse.loading<Addpeopleingroup>()) }
                ?.let {
                    disposables.add(
                        it
                            .subscribe(
                                { result ->
                                    Log.e(HomeViewModel.TAG, "ProductsAssigned()")
                                    Log.e("ProductsAssigned()", result.toString())
                                    responseLiveDatahome.setValue(ApiResponse.success(result))
                                },
                                { throwable ->
                                    //val responseCode = (throwable as HttpException).code()
                                    //Log.e(TAG, "responseCode() - $responseCode")
                                    responseLiveDatahome.setValue(
                                        ApiResponse.error<Addpeopleingroup>(
                                            throwable
                                        )
                                    )
                                }
                            ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "responseLiveProductsAssigned()-Exception-" + ex.message)
        }
    }

    /*Upload profile photot*/

    fun getuploadeprofileview(): MutableLiveData<ApiResponse<*>>? {
        return responseLiveDatauploadImage
    }
    fun uploadProfilePhoto(file: File, token: String) {
        Log.e(HomeViewModel.TAG, file.toString())
        Log.e(HomeViewModel.TAG, "responseLiveupload-" + file + " "+ token)

//        file = file.split("/")[file.split("/").length - 1];
        try {
//            jsonBody.put("fileType", "image/jpeg");
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
            repository.uploadProfileImage(body, token)
                ?.enqueue(object : Callback<UploadResponse?> {
                    override fun onResponse(
                        call: Call<UploadResponse?>,
                        response: Response<UploadResponse?>
                    ) {
                        println("@response" + response)
                        println("@responsevalue" + response().value)
                        println("@responsebreak" + response())
                        println("@UploadResponse" + response.body())
                        println("@responseraw" + response.raw())
                        println("@responsemessges" + response.message())


                        if (response.code() == 422 || response.code() == 403 || response.code() == 500) {
                            println("error422" + response.errorBody())
                            utility!!.showToast(
                                            context,
                                            ("Something went wrong Please try again later")
                                        )
//                            responseLiveDatauploadImage.postValue(ApiResponse.error(response.errorBody()))
                        }
                        if (response.code() == 200 || response.code() == 201) {
                            println("profileresponse" + response.body())
                            responseLiveDatauploadImage.postValue(ApiResponse.success(response.body()))
                        }
                    }

                    override fun onFailure(call: Call<UploadResponse?>, t: Throwable) {
                        println("errorgot" + t)
                        utility!!.showToast(
                            context,
                            ("Something went wrong Please try again later")
                        )
        //                    onError(t);
//                        responseLiveDatauploadImage.postValue(ApiResponse)
                    }
                })
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
            utility!!.showToast(
                context,
                ("Something went wrong Please try again later")
            )
            Log.e(
               HomeViewModel.TAG,
                "updateProfile()-Excp-" + ex.message

            )
        }


//        try {
//            repository.uploadrequest(body, token)
//                ?.subscribeOn(Schedulers.io())
//                ?.observeOn(AndroidSchedulers.mainThread())
//                ?.doOnSubscribe { responseLiveDatauploadImage.setValue(ApiResponse.loading<UploadResponse>()) }
//                ?.let {
//                    disposables.add(
//                        it
//                            .subscribe(
//                                { result ->
//                                    Log.e(HomeViewModel.TAG, "ProductsAssigned()")
//                                    Log.e("ProductsAssigned()", result.toString())
//                                    responseLiveDatauploadImage.setValue(ApiResponse.success(result))
//                                },
//                                { throwable ->
//                                    //val responseCode = (throwable as HttpException).code()
//                                    //Log.e(TAG, "responseCode() - $responseCode")
//                                    responseLiveDatauploadImage.setValue(
//                                        ApiResponse.error<UploadResponse>(
//                                            throwable
//                                        )
//                                    )
//                                }
//                            ))
//                }
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//            Log.e(HomeViewModel.TAG, "responseLiveProductsAssigned()-Exception-" + ex.message)
//        }
    }

    /*Upload proof photo*/

    fun getuploadeproofview(): MutableLiveData<ApiResponse<*>>? {
        return responseLiveDatauploadImage
    }

    fun uploadProofPhoto(file: File, token: String) {
        Log.e(HomeViewModel.TAG, file.toString())
        Log.e(HomeViewModel.TAG, "responseLiveupload-" + file + " "+ token)

//        file = file.split("/")[file.split("/").length - 1];
        try {
//            jsonBody.put("fileType", "image/jpeg");
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
            repository.uploadProofImage(body, token)
                ?.enqueue(object : Callback<UploadResponse?> {
                    override fun onResponse(
                        call: Call<UploadResponse?>,
                        response: Response<UploadResponse?>
                    ) {
                        println("@response" + response)
                        println("@responsevalue" + response().value)
                        println("@responsebreak" + response())
                        println("@UploadResponse" + response.body())
                        println("@responseraw" + response.raw())
                        println("@responsemessges" + response.message())


                        if (response.code() == 422 || response.code() == 403 || response.code() == 500) {
                            println("error422" + response.errorBody())
                            utility!!.showToast(
                                context,
                                ("Something went wrong Please try again later")
                            )
//                            responseLiveDatauploadImage.postValue(ApiResponse.error(response.errorBody()))
                        }
                        if (response.code() == 200 || response.code() == 201) {
                            println("profileresponse" + response.body())
                            responseLiveDatauploadImage.postValue(ApiResponse.success(response.body()))
                        }
                    }

                    override fun onFailure(call: Call<UploadResponse?>, t: Throwable) {
                        println("errorgot" + t)
                        utility!!.showToast(
                            context,
                            ("Something went wrong Please try again later")
                        )
                        //                    onError(t);
//                        responseLiveDatauploadImage.postValue(ApiResponse)
                    }
                })
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()

            Log.e(
                HomeViewModel.TAG,
                "updateProfile()-Excp-" + ex.message

            )
        }


//        try {
//            repository.uploadrequest(body, token)
//                ?.subscribeOn(Schedulers.io())
//                ?.observeOn(AndroidSchedulers.mainThread())
//                ?.doOnSubscribe { responseLiveDatauploadImage.setValue(ApiResponse.loading<UploadResponse>()) }
//                ?.let {
//                    disposables.add(
//                        it
//                            .subscribe(
//                                { result ->
//                                    Log.e(HomeViewModel.TAG, "ProductsAssigned()")
//                                    Log.e("ProductsAssigned()", result.toString())
//                                    responseLiveDatauploadImage.setValue(ApiResponse.success(result))
//                                },
//                                { throwable ->
//                                    //val responseCode = (throwable as HttpException).code()
//                                    //Log.e(TAG, "responseCode() - $responseCode")
//                                    responseLiveDatauploadImage.setValue(
//                                        ApiResponse.error<UploadResponse>(
//                                            throwable
//                                        )
//                                    )
//                                }
//                            ))
//                }
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//            Log.e(HomeViewModel.TAG, "responseLiveProductsAssigned()-Exception-" + ex.message)
//        }
    }

    /*Add to Compare Api*/
    private val getassignworkData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getAssignWorkResponse(): MutableLiveData<ApiResponse<*>>{
        return getassignworkData
    }

    fun AssignDailyWork(assignproRequest : AssignProductRequest, token: String) {
        Log.e(TAG,"AssignDailyWork()")
        try {
            repository.assignWorkrequest(assignproRequest, token,"application/json")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getassignworkData.setValue(ApiResponse.loading<AssignProductResponse>()) }
                ?.let {
                    disposables.add(
                        it
                            .subscribe(
                                { result ->
                                    Log.e(HomeViewModel.TAG, "ProductsAssigned()")
                                    Log.e("ProductsAssigned()", result.toString())
                                    getassignworkData.setValue(ApiResponse.success(result))
                                },
                                { throwable ->
                                    //val responseCode = (throwable as HttpException).code()
                                    //Log.e(TAG, "responseCode() - $responseCode")
                                    getassignworkData.setValue(
                                        ApiResponse.error<AssignProductResponse>(
                                            throwable
                                        )
                                    )
                                }
                            ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "responseLiveProductsAssigned()-Exception-" + ex.message)
        }
    }

    /*work assigned user list*/
    private val responseLiveworkassignedUsers = MutableLiveData<ApiResponse<*>>()

    fun getworkassigneduserResponse(): MutableLiveData<ApiResponse<*>>? {
        return responseLiveworkassignedUsers
    }
    fun workassigneduserrequest(grpId : String, vendorId: Int, s: String) {
        Log.e(HomeViewModel.TAG,  s)
        try {
            repository.workassignedlistrequest(grpId,vendorId,s)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { responseLiveworkassignedUsers.setValue(ApiResponse.loading<WorkAssignedUserModelResponse>()) }
                ?.let {
                    disposables.add(
                        it
                            .subscribe(
                                { result ->
                                    Log.e(HomeViewModel.TAG, "ProductsAssigned()")
                                    Log.e("ProductsAssigned()", result.toString())
                                    responseLiveworkassignedUsers.setValue(ApiResponse.success(result))
                                },
                                { throwable ->
                                    //val responseCode = (throwable as HttpException).code()
                                    //Log.e(TAG, "responseCode() - $responseCode")
                                    responseLiveworkassignedUsers.setValue(
                                        ApiResponse.error<WorkAssignedUserModelResponse>(
                                            throwable
                                        )
                                    )
                                }
                            ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "responseLiveProductsAssigned()-Exception-" + ex.message)
        }

    }

    /*unassign User*/
    private val unassignUserData = MutableLiveData<ApiResponse<*>>()

    fun unassignUserRequest(): MutableLiveData<ApiResponse<*>>? {
        return unassignUserData
    }

    fun unassignUserResponse(userId: String, s: String) {
        Log.e("unassignUserResponse",  s)
        try {
            repository.unassignUserRequest(userId, s,"application/json")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { unassignUserData.setValue(ApiResponse.loading<UnassignResponseModel>()) }
                ?.let {
                    disposables.add(
                        it
                            .subscribe(
                                { result ->
                                    Log.e(HomeViewModel.TAG, "UnAssigned()")
                                    Log.e("UnAssigned()", result.toString())
                                    unassignUserData.setValue(ApiResponse.success(result))
                                },
                                { throwable ->
                                    //val responseCode = (throwable as HttpException).code()
                                    //Log.e(TAG, "responseCode() - $responseCode")
                                    unassignUserData.setValue(
                                        ApiResponse.error<UnassignResponseModel>(
                                            throwable
                                        )
                                    )
                                }
                            ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "UnAssign User()-Exception-" + ex.message)
        }

    }

    /*update grpdata*/
    private val updateGroupData = MutableLiveData<ApiResponse<*>>()

    fun updateGroupDataRequest(): MutableLiveData<ApiResponse<*>>? {
        return updateGroupData
    }

    fun updateGroupDataResponse(updateUserGroupRequest: UpdateUserGroupRequest, s: String) {
        Log.e("updateGroupDataResponse",  s)
        try {
            repository.updateGrpRequest(updateUserGroupRequest, s,"application/json")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { updateGroupData.setValue(ApiResponse.loading<UpdateUserGroupResponse>()) }
                ?.let {
                    disposables.add(
                        it
                            .subscribe(
                                { result ->
                                    Log.e(HomeViewModel.TAG, "ProductsAssigned()")
                                    Log.e("ProductsAssigned()", result.toString())
                                    updateGroupData.setValue(ApiResponse.success(result))
                                },
                                { throwable ->
                                    //val responseCode = (throwable as HttpException).code()
                                    //Log.e(TAG, "responseCode() - $responseCode")
                                    updateGroupData.setValue(
                                        ApiResponse.error<UpdateUserGroupResponse>(
                                            throwable
                                        )
                                    )
                                }
                            ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "responseLiveProductsAssigned()-Exception-" + ex.message)
        }

    }

    /*update people data*/
    private val updatePeopleData = MutableLiveData<ApiResponse<*>>()

    fun updatePeopleDataRequest(): MutableLiveData<ApiResponse<*>>? {
        return updatePeopleData
    }

    fun updatePeopleDataResponse(id : String,grpId : String, status : String, adharnum : String, name: String, s: String) {
        Log.e("updatePeopleDataResponse",  s)
        try {
            repository.updatePeopleRequest(id, grpId,status,adharnum,name, s,"application/json")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { updatePeopleData.setValue(ApiResponse.loading<UpdatePeopleResponse>()) }
                ?.let {
                    disposables.add(
                        it
                            .subscribe(
                                { result ->
                                    Log.e(HomeViewModel.TAG, "UpdatePeople()")
                                    Log.e("UpdatePeople()", result.toString())
                                    updatePeopleData.setValue(ApiResponse.success(result))
                                },
                                { throwable ->
                                    //val responseCode = (throwable as HttpException).code()
                                    //Log.e(TAG, "responseCode() - $responseCode")
                                    updatePeopleData.setValue(
                                        ApiResponse.error<UpdatePeopleResponse>(
                                            throwable
                                        )
                                    )
                                }
                            ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "responseLiveProductsAssigned()-Exception-" + ex.message)
        }

    }

    /*Productwise User list data*/
    private val productwiseData = MutableLiveData<ApiResponse<*>>()

    fun productwiseDataRequest(): MutableLiveData<ApiResponse<*>>? {
        return productwiseData
    }

    fun productwiseDataResponse(transId: String, s: String) {
        Log.e("productwiseDataResponse",  s)
        try {
            repository.productWiseUserListRequest(transId, s,"application/json")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { productwiseData.setValue(ApiResponse.loading<UpdatePeopleResponse>()) }
                ?.let {
                    disposables.add(
                        it
                            .subscribe(
                                { result ->
                                    Log.e(HomeViewModel.TAG, "UpdatePeople()")
                                    Log.e("UpdatePeople()", result.toString())
                                    productwiseData.setValue(ApiResponse.success(result))
                                },
                                { throwable ->
                                    //val responseCode = (throwable as HttpException).code()
                                    //Log.e(TAG, "responseCode() - $responseCode")
                                    productwiseData.setValue(
                                        ApiResponse.error<UpdatePeopleResponse>(
                                            throwable
                                        )
                                    )
                                }
                            ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "responseLiveProductsAssigned()-Exception-" + ex.message)
        }

    }

    /*Send Transaction data*/
    private val transactionData = MutableLiveData<ApiResponse<*>>()

    fun transactionDataRequest(): MutableLiveData<ApiResponse<*>>? {
        return transactionData
    }

    fun transactionDataResponse(transRequest: TransactionRequestModel, s: String) {
        Log.e("transactionDataResponse",  s)
        try {
            repository.transactionRequest(transRequest, s,"application/json")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { transactionData.setValue(ApiResponse.loading<TransactionResponseModel>()) }
                ?.let {
                    disposables.add(
                        it
                            .subscribe(
                                { result ->
                                    Log.e(HomeViewModel.TAG, "UpdatePeople()")
                                    Log.e("UpdatePeople()", result.toString())
                                    transactionData.setValue(ApiResponse.success(result))
                                },
                                { throwable ->
                                    //val responseCode = (throwable as HttpException).code()
                                    //Log.e(TAG, "responseCode() - $responseCode")
                                    transactionData.setValue(
                                        ApiResponse.error<TransactionResponseModel>(
                                            throwable
                                        )
                                    )
                                }
                            ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "responseLiveProductsAssigned()-Exception-" + ex.message)
        }

    }


}

