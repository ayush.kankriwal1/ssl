package com.absyz.ssl.cameraview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.absyz.kotlin.dagger.ApplicationController;
import com.absyz.ssl.R;
import com.checkin.helpers.SessionStore;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class UtilityJava {

    private static final String TAG = "ImageZoomCrop";

    public static void e(Throwable e){
        Log.e(TAG,e.getMessage(),e);
    }

    public static void e(String msg){
        Log.e(TAG,msg);
    }
    public static Uri getImageUri(String path) {
        return Uri.fromFile(new File(path));
    }



    public interface IntentExtras{
        String ACTION_CAMERA = "action-camera";
        String ACTION_GALLERY = "action-gallery";
        String IMAGE_PATH = "image-path";
    }

    public interface PicModes{
        String CAMERA = "Camera";
        String GALLERY = "Gallery";
    }

    //    public UtilityJava(Context context) {
//    }
    private static Gson gson = new Gson();
    SessionStore sessionStore;

    public UtilityJava(Context context) {
        Log.e("UtilityJava", " initialized-");
        ((ApplicationController) context).getApplicationComponent().inject(sessionStore);
    }

    public static Gson getGson() {
        return gson;
    }


    public String saveImageGallery(Context context, Bitmap bitmap, String filename, String imgActualPath) {
        File file = null, galleryDirectory, directory;
        String root;
        byte[] byteArrayData;
        Bitmap localBitMap;
        FileOutputStream fos;
        boolean isGallery = false;
        try {
            root = Environment.getExternalStorageDirectory().toString();
            directory = new File(root + "/" + context.getString(R.string.app_name));
            if (!directory.exists())
                directory.mkdirs();

           /* galleryDirectory = directory.getParentFile();
            galleryDirectory = new File(directory + "/" + context.getString(R.string.app_name) + " Images");
            if (!galleryDirectory.exists())
                galleryDirectory.mkdirs();*/

            if (filename != null) {
                if (filename.endsWith(".jpg")
                        || filename.endsWith(".png")
                        || filename.endsWith(".jpeg")) {

                    file = new File(directory, filename);
                    isGallery = true;
                }
            }
            if (file != null && file.exists())
                file.delete();
            try {
                if (file != null) {
                    fos = new FileOutputStream(file, false);
                    if (isGallery) {
                        if (imgActualPath != null) {
                            localBitMap = BitmapFactory.decodeFile(imgActualPath);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            localBitMap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            byteArrayData = stream.toByteArray();
                            fos.write(byteArrayData);
                        } else
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 95, fos);
                    }
                    fos.flush();
                    fos.close();

                    MediaScannerConnection
                            .scanFile(context, new String[]{file.getAbsolutePath()},
                                    new String[]{"image/jpeg"}, null);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            } catch (OutOfMemoryError ex) {
                ex.printStackTrace();
            }

            refreshGallery(context, directory + "/" + filename);
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return "";
    }
    private void refreshGallery(Context context, String url) {
        try {
            File file = new File(url);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File f = new File("file://" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES));
                Uri contentUri = Uri.fromFile(file);
                mediaScanIntent.setData(contentUri);
                context.sendBroadcast(mediaScanIntent);
            } else {
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(url)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



    public void showToast(Context context, String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        try {
            if (height > reqHeight || width > reqWidth) {
                final int heightRatio = Math.round((float) height / (float) reqHeight);
                final int widthRatio = Math.round((float) width / (float) reqWidth);
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            }
            final float totalPixels = width * height;
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return inSampleSize;
    }

}
