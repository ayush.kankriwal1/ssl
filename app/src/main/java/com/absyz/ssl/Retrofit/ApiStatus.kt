package com.checkin.network

/**
 * Created by Ayush on 30-05-2021.
 */

enum class ApiStatus {
    LOADING,
    SUCCESS,
    ERROR,
    ERRORRESPONSE

}