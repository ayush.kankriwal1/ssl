package com.checkin.network

import com.checkin.network.ApiStatus.*
import io.reactivex.annotations.NonNull
import io.reactivex.annotations.Nullable
import okhttp3.ResponseBody


/**
 * Created by Ayush on 30-05-2021.
 */
class ApiResponse<T> private constructor(
    var apiStatus: ApiStatus, @param:Nullable @field:Nullable
    var data: T?, @param:Nullable @field:Nullable
    var error: Throwable?,
)

{
    private fun ApiResponse(
        apiStatus: ApiStatus,
        data: T?,
        error: Throwable?,
        responseBody: ResponseBody
    ) {
        this.apiStatus = apiStatus
        this.data = data
        this.error = error
    }

    companion object {

        fun <T> loading(): ApiResponse<T> {
            return ApiResponse(LOADING, null, null)
        }

        fun <T> success(@NonNull data: T): ApiResponse<T> {
            return ApiResponse(SUCCESS, data, null)
        }

        fun <T> error(@NonNull error: Throwable): ApiResponse<T> {
            return ApiResponse(ERROR, null, error)
        }



    }

}
