package com.checkin.network

import com.absyz.kotlin.Retrofit.NetworkURI
import com.absyz.ssl.model.*

import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

//    @GET()
//    fun userLogindetails(
//        @Url s: String?,
//        @Header("Authorization") authToken: String?,
//    ): Observable<UserDetailResponseModel?>?
//
    @POST(NetworkURI.auth)
    fun loginauth(
//        @Header("Authorization") authToken: String?,
        @Body body: Loginmodel
    ): Observable<LoginResponseModel?>?

//    @GET(NetworkURI.vendoreList)
//    fun vendoreListgroup(
//        @Header("Authorization") authToken: String?,
//        @Header("Content-Type") contentType: String?,
//        @Query ("vendorId") vendorId: Int?,
//        ): Observable<Homemodel>
//

    @GET()
    fun vendoreListgroup(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
    ): Observable<HomeViewModelResponse>


    @GET()
    fun productListgroup(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
    ): Observable<ProductsAssigned>

    @DELETE()
    fun unassignUser(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
    ): Observable<UnassignResponseModel>

    @PUT
    fun updateGrp(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
        @Body body : UpdateUserGroupRequest,
    ): Observable<UpdateUserGroupResponse>

    @PUT
    fun updatePeople(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
    ): Observable<UpdatePeopleResponse>


    @GET()
    fun groupofpeopleListgroup(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
    ): Observable<GroupOfPeopleModel>

    @GET()
    fun ProductWiseListgroup(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
    ): Observable<ProductWiseUserListResponse>

    @GET()
    fun workassignedListgroup(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
    ): Observable<WorkAssignedUserModelResponse>

    @POST()
    fun transactionValue(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
        @Body body : TransactionRequestModel
    ): Observable<TransactionResponseModel>

    @POST()
    fun addpeopleListgroup(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
    ): Observable<Addpeopleingroup>

    @Multipart
    @POST()
    fun uploadProfilePhoto(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
        @Part body: MultipartBody.Part?,
    ): Observable<UploadResponse>

    @POST()
    fun assignWorkRequest(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
        @Body body : AssignProductRequest,
    ): Observable<AssignProductResponse>

    /*
     */
    /* api to send link for edit profile */ /*

    @Multipart
    @POST
    Call<ResponseBody>
    getUpdateprofile(@Url String url,
                     @Header("Authorization") String value,
                     @Part() MultipartBody.Part logo);
*/
    @Multipart
    @POST
    fun getUpdateprofile(
        @Url url: String?,
        @Header("Authorization") value: String?,
        @Part file: MultipartBody.Part?,
    ): Call<UploadResponse?>?

    @Multipart
    @POST
    fun getUpdateproof(
        @Url url: String?,
        @Header("Authorization") value: String?,
        @Part file: MultipartBody.Part?,
    ): Call<UploadResponse?>?


}
