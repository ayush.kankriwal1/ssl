package com.absyz.kotlin.Retrofit

object NetworkURI {

    /*POST*/
    const val auth = "authenticate"
    const val vendoreList = "getGroupsByVendorId"
    const val productList = "productsAssigned"
    const val peopleinthegroup = "getVolunteersByGroupId"
    const val createVolunteer = "createVolunteer"
    const val upload = "file/upload"
    const val assignwork = "assignDailyWork"
    const val workassigneduserapi = "getDailyWork"
    const val unassignwork = "unAssignUserFromDailyWork"
    const val updateusergrp = "updateUserGroup"
    const val updatepeoplegrp = "updateVolunteer"
    const val productwiseuserapi = "getWorkAssignedUsersbasedOntransId"
    const val dailyTransations = "dailyTransaction2"

    /*GET*/

}