package com.checkin.network

import com.absyz.kotlin.Retrofit.NetworkURI
import com.absyz.ssl.model.*
import com.checkin.helpers.AppConstants

import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.HTTP

class Repository(private val apiService: ApiService) {

//    fun getUserdetails(authtoken: String?): Observable<UserDetailResponseModel?>? =
//        apiService.userLogindetails(AppConstants.BaseURL + NetworkURI.userdetails,
//            authtoken)

    fun validateUser(request: Loginmodel): Observable<LoginResponseModel?>? =
        apiService.loginauth(request)


    fun requestvendore(userId: Int?, token: String, type: String): Observable<HomeViewModelResponse> =
        apiService.vendoreListgroup(AppConstants.BaseUrl+NetworkURI.vendoreList+"?vendorId="+userId, token, type)

    fun productlistrequest(userId: Int?, token: String, type: String): Observable<ProductsAssigned> =
        apiService.productListgroup(AppConstants.BaseUrl2+NetworkURI.productList+"?vendor_id="+userId, token, type)

    fun grouplistrequest(userId: String?, token: String, type: String): Observable<GroupOfPeopleModel> =
        apiService.groupofpeopleListgroup(AppConstants.BaseUrl+NetworkURI.peopleinthegroup+"?group_id="+userId, token, type)

    fun workassignedlistrequest(userId: String?, vendorId: Int, token: String): Observable<WorkAssignedUserModelResponse> =
        apiService.workassignedListgroup(AppConstants.BaseUrl2+NetworkURI.workassigneduserapi+"?vendorId="+vendorId+"&groupId="+userId, token)

    fun addpeoplelistrequest(adharnumber: String?, groupid: String, voluname: String, profPic : String, proofPic : String, type: String, token: String): Observable<Addpeopleingroup> =
        apiService.addpeopleListgroup(AppConstants.BaseUrl+NetworkURI.createVolunteer+"?adhar_no="+adharnumber+"&"+"group_id"+"="+groupid + "&name=" + voluname+"&profile_pic=" + profPic+"&proof_pic="+ proofPic, token)

    fun uploadrequest(body: MultipartBody.Part, token: String): Observable<UploadResponse> =
        apiService.uploadProfilePhoto(AppConstants.BaseUrl+NetworkURI.upload, token, body)

    /*Profile Image*/
    fun uploadProfileImage(fileUrl: MultipartBody.Part?, token: String?): Call<UploadResponse?>? {
        return apiService.getUpdateprofile(
            AppConstants.BaseUrl + NetworkURI.upload,
            token,
            fileUrl,
        )
    }

    /*Proof Image*/
    fun uploadProofImage(fileUrl: MultipartBody.Part?, token: String?): Call<UploadResponse?>? {
        return apiService.getUpdateproof(
            AppConstants.BaseUrl + NetworkURI.upload,
            token,
            fileUrl,
        )
    }

    @HTTP(method = "DELETE", path = "https://ssi-admin-portal-nodejs.herokuapp.com/transactions/unAssignUserFromDailyWork", hasBody = true)
    fun unassignUserRequest(userId : String, token: String, type: String): Observable<UnassignResponseModel> =
        apiService.unassignUser(AppConstants.BaseUrl2+NetworkURI.unassignwork+"?id="+userId, token, type)

    fun assignWorkrequest(assignProductRequest: AssignProductRequest, token: String, type: String): Observable<AssignProductResponse> =
        apiService.assignWorkRequest(AppConstants.BaseUrl2+NetworkURI.assignwork, token,type,assignProductRequest)

    fun updateGrpRequest(updateUserGroupRequest: UpdateUserGroupRequest, token: String, type: String): Observable<UpdateUserGroupResponse> =
        apiService.updateGrp(AppConstants.BaseUrl+NetworkURI.updateusergrp, token, type,updateUserGroupRequest)

    fun updatePeopleRequest(id: String, grpId : String, status:String, adharnumber: String,name : String, token: String, type: String): Observable<UpdatePeopleResponse> =
        apiService.updatePeople(AppConstants.BaseUrl+NetworkURI.updatepeoplegrp+"?adhar_no="+adharnumber+"&group_id="+grpId+"&id="+id+"&name="+name+"&status="+status, token, type)

    fun productWiseUserListRequest(transId: String, token: String, type: String): Observable<ProductWiseUserListResponse> =
        apiService.ProductWiseListgroup(AppConstants.BaseUrl2+NetworkURI.productwiseuserapi+"?transId="+transId, token, type)

    fun transactionRequest(transRequest: TransactionRequestModel, token: String, type: String): Observable<TransactionResponseModel> =
        apiService.transactionValue(AppConstants.BaseUrl2+NetworkURI.dailyTransations, token, type, transRequest)
}
