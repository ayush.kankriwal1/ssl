package com.absyz.kotlin.Retrofit

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}