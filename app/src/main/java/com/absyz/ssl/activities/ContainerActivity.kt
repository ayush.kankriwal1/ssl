package com.absyz.ssl.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.absyz.ssl.R
import com.absyz.ssl.databinding.ActivityContainerBinding
import com.absyz.ssl.model.ProductsAssigned
import com.absyz.ssl.model.ResultProduct
import com.checkin.helpers.AppConstants
import fragment.*
import org.apache.commons.lang3.StringUtils


class ContainerActivity :BaseActivity() {

    lateinit var binding: ActivityContainerBinding
    var title: String = ""
    var groupID:String? = null
    var bundle : Bundle? = null

    var logoutView: MenuItem? = null


    companion object {

        const val TAG = "ContainerActivity"

        fun gotoContainerActivity(context: Context, title: String = "", bundle: Bundle) {
            val intent = Intent(context, ContainerActivity::class.java)
            intent.putExtra(AppConstants.SCREEN_NAME, title)
            intent.putExtras(bundle);
            context.startActivity(intent)
        }

//        fun gotoViewQrCode(
//            context: Context,
//            title: String = "",
//            base64: String = "",
//            searchItem: SearchItem?
//        ) {
//            val intent = Intent(context, ContainerActivity::class.java)
//            intent.putExtra(AppConstants.SCREEN_NAME, title)
//            intent.putExtra(AppConstants.ITEM, searchItem)
//            intent.putExtra(AppConstants.BASE64, base64)
//            context.startActivity(intent)
//        }

    }




    override fun onBackPressed() {
        try {
            if (supportFragmentManager.backStackEntryCount > 1) {
                supportFragmentManager.popBackStack()
            } else {
                super.onBackPressed()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            binding = DataBindingUtil.setContentView(this, R.layout.activity_container)


// getting the bundle back from the android

// performing the safety null check

// getting the string back

            bundle= intent.extras

            if (intent.getStringExtra(AppConstants.SCREEN_NAME)=== null)
                title = "Welcome"
            else
                title = intent.getStringExtra(AppConstants.SCREEN_NAME)!!

            // getting the bundle back from the android


// getting the string back

// getting the string back


            setSupportActionBar(binding.toolbar.toolbar)

            findFragment()

            Log.e(TAG, "onCreate()")

        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "nullbundle()")

        }

    }

    private fun findFragment() {
        try {

            binding.toolbar.toolbarTitle.text = title
            title.let {
                when {
                    StringUtils.equalsIgnoreCase(
                        title,
                        getString(R.string.welcome)
                    ) -> {
                        setFragment(LoginFragment())
                    }

                    StringUtils.equalsIgnoreCase(
                        title,
                        getString(R.string.home)
                    ) -> setFragment(HomeFragment())

                    StringUtils.equalsIgnoreCase(
                        title,
                        getString(R.string.addpeople)
                    ) -> {
                        val viewQrFragment = AddPeopleFragment();
                        viewQrFragment.arguments = bundle
                        setFragment(viewQrFragment)
                    }

//                  StringUtils.equalsIgnoreCase(
//                        title,
//                        getString(R.string.group)
//                    ) ->{
//                      val viewQrFragment = GroupofPeople("");
//                      viewQrFragment.arguments = bundle
//                      setFragment(viewQrFragment)
//                  }

//                    StringUtils.equalsIgnoreCase(
//                        title,
//                        getString(R.string.assignproduct)
//                    ) ->{
//                        val viewQrFragment = AssignProductFragment()
//                        viewQrFragment.arguments = bundle
//                        setFragment(AssignProductFragment())
//                    }

                }
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
    private fun setFragment(fragment: Fragment) {
        try {
            val manager = supportFragmentManager
            val fragmentTransaction = manager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, fragment, null)
            fragmentTransaction.commitAllowingStateLoss()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }



}