package com.absyz.ssl.activities

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.ssl.R
import com.absyz.ssl.databinding.ActivityEditpeopleBinding
import com.absyz.ssl.model.HomeViewModelResponse
import com.absyz.ssl.model.LoginResponseModel
import com.absyz.ssl.model.UpdatePeopleResponse
import com.absyz.ssl.model.UpdateUserGroupRequest
import com.absyz.ssl.viewmodel.HomeViewModel
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import fragment.HomeFragment
import java.net.URI
import android.graphics.BitmapFactory

import android.graphics.Bitmap

import java.net.URL


class EditPeopleActivity: BaseActivity() {

    lateinit var binding : ActivityEditpeopleBinding
    var id: Int = 0
    var grpId: Int = 0
    var status: Int = 0
    var adharno: String = ""
    var name: String = ""
    var profpic: String=""
    var proofpic : String = ""
    var message : String = ""
    //lateinit var profUri : Uri
    //lateinit var proofUri : Uri

    lateinit var viewModel: HomeViewModel
    var userDetails: LoginResponseModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_editpeople)

        id = intent.getIntExtra("id", 1)
        grpId = intent.getIntExtra("grpid", 1)
        status = intent.getIntExtra("status", 1)
        adharno = intent.getStringExtra("adharno").toString()
        name = intent.getStringExtra("name").toString()
        profpic = intent.getStringExtra("profpic").toString()
        proofpic = intent.getStringExtra("proofpic").toString()

        //profUri = Uri.parse(profpic)
        //proofUri = Uri.parse(proofpic)

        val proof_url = URL(proofpic)
        val proof_bit = BitmapFactory.decodeStream(proof_url.openConnection().getInputStream())

        val prof_url = URL(profpic)
        val prof_bit = BitmapFactory.decodeStream(prof_url.openConnection().getInputStream())


        (application as ApplicationController).applicationComponent.inject(this)

        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        //Update People data
        viewModel.updatePeopleDataRequest()?.observe(this, { this.consumePeopleResponse(it as ApiResponse<*>) })

        //Call api
        binding.etname.setText(name)
        binding.etAdhar.setText(adharno)
        binding.uploadprofilpic.setImageBitmap(prof_bit)
        binding.uploadProofView.setImageBitmap(proof_bit)
        binding.button1.setOnClickListener() {
            updatepeopleapiCall(id,grpId,status,binding.etAdhar.text.toString(),binding.etname.text.toString())
        }
    }

    private fun consumePeopleResponse(response: ApiResponse<*>) {
        Log.e(HomeFragment.TAG, "consumePeopleResponse()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(HomeFragment.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is UpdatePeopleResponse) {
//                                sessionStore!!.saveUserDetails(it)
                                message = it.message.toString()
                                Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
                                finish()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        Log.e(HomeFragment.TAG, "consumeResponse()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(HomeFragment.TAG, "consumeResponseone() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeFragment.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }


    private fun updatepeopleapiCall(
        id : Int,
        grpId : Int,
        status : Int,
        adharnum : String,
        name : String,
    ) {
        try {
            userDetails = sessionStore!!.getUserDetails()
            val e = Log.e(BaseActivity.TAG, "updategrpapiCall - " + userDetails)
            viewModel.updatePeopleDataResponse(id.toString(),grpId.toString(),status.toString(),adharnum,name, ("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

}