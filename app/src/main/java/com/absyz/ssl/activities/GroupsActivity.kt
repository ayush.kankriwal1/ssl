package com.absyz.ssl.activities

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.absyz.ssl.R
import com.absyz.ssl.adapter.GroupTabPagerAdapter
import com.absyz.ssl.databinding.ActivityGroupBinding
import com.google.android.material.tabs.TabLayout
import fragment.AllGroupofPeople




class GroupsActivity : BaseActivity() {

    lateinit var binding: ActivityGroupBinding

    var grp_id: String = ""
    //lateinit var allfragment : AllGroupofPeople

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {

            binding = DataBindingUtil.setContentView(this, R.layout.activity_group)

            grp_id = intent.getIntExtra("grpId",1).toString()
            Log.d("grp_id:", grp_id)

            val fragmentAdapter = GroupTabPagerAdapter(supportFragmentManager)
            binding.viewpagerMain.adapter = fragmentAdapter
            binding.tabsMain.setupWithViewPager(binding.viewpagerMain)

            fragmentAdapter.getgroupId(grp_id)

            //val b = Bundle()
            //b.putString("grp_Id", grp_id)
            //allfragment.setArguments(b)
            //fragmentTransaction.add(R.id.frameLayout, myFragment).commit()

        } catch(ex: Exception){
            ex.printStackTrace();
        }

    }
}

