package com.absyz.ssl.activities

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.ssl.R
import com.absyz.ssl.databinding.ActivityEditvendorBinding
import com.absyz.ssl.model.*
import com.absyz.ssl.viewmodel.HomeViewModel
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import fragment.AllGroupofPeople

class EditVendorActivity: BaseActivity() {

    lateinit var binding : ActivityEditvendorBinding
    var grpid: Int = 0
    var grpname: String = ""
    var createdDate: String=""
    var date : Int = 0
    var message : String = ""
    lateinit var updateUserGroupRequest: UpdateUserGroupRequest

    lateinit var viewModel: HomeViewModel
    var userDetails: LoginResponseModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_editvendor)

        grpid = intent.getIntExtra("id", 1)
        grpname = intent.getStringExtra("grpname").toString()
        createdDate = intent.getStringExtra("createddate").toString()
        Log.d("CreateDate:",createdDate )
        Toast.makeText(this,"CreateDate: " + createdDate, Toast.LENGTH_SHORT).show()

        (application as ApplicationController).applicationComponent.inject(this)

        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        //pass vendor grp ID and fetch people list
        viewModel.updateGroupDataRequest()!!.observe(this, { this.consumeUpdatedGroupResponse(it as ApiResponse<*>) })

        binding.grpidVal.text = grpid.toString()
        binding.createddateVal.text = createdDate
        binding.grpname.setText(grpname)
        updateUserGroupRequest = UpdateUserGroupRequest(
            createDate  = createdDate,
            groupName = binding.grpname.text.toString(),
            id = grpid
        )

        binding.submitBtn.setOnClickListener() {
            updategrpapiCall(updateUserGroupRequest)
;        }
    }

    private fun updategrpapiCall(updateUserGroupRequest: UpdateUserGroupRequest) {
        try {
            userDetails = sessionStore!!.getUserDetails()

            val e = Log.e(BaseActivity.TAG, "updategrpapiCall - " + userDetails)

            viewModel.updateGroupDataResponse(updateUserGroupRequest, ("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    private fun consumeUpdatedGroupResponse(response: ApiResponse<*>) {
        Log.e(AllGroupofPeople.TAG, "consumePeopleResponse()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(AllGroupofPeople.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is UpdateUserGroupResponse) {
//                                sessionStore!!.saveUserDetails(it)
                                message = it.message.toString()
                                Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
                                finish()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        /*val apiError =
                            errorUtils!!.parseError((response.error as HttpException).response())*/
                        //Log.e(TAG, "consumeResponse() apiError - ${apiError!!.error_description}")
                        //(response.error as HttpException).response().errorBody()
                        Toast.makeText(this,response.error!!.message!! , Toast.LENGTH_SHORT).show()
                        Log.e("UpdateUserGroup", "consumeUpdateGroupResponse1()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e("UpdateUserGroup", "consumeUpdateGroupResponse2() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e("UpdateUserGroup", "consumeUpdateGroupResponse3() Exceptionmanin - ${ex.message}")
        }

    }


}