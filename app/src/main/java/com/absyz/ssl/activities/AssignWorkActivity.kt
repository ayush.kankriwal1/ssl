package com.absyz.ssl.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.ssl.R
import com.absyz.ssl.databinding.ActivityAssignproductBinding
import com.absyz.ssl.model.*
import com.absyz.ssl.viewmodel.HomeViewModel
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import fragment.AllGroupofPeople
import fragment.HomeFragment
import kotlinx.android.synthetic.main.activity_assignproduct.*

class AssignWorkActivity : BaseActivity() {

    lateinit var binding: ActivityAssignproductBinding
    private var vendoregrouplist: ArrayList<Resultdata> = ArrayList()
    private var peoplelist: ArrayList<ResultGroup> = ArrayList()

    var trans_id: Int = 0
    var prodname: String = ""
    var selected_grpname : String = "1"
    var selected_grpID : String = ""
    var selected_pplname : String = "1"
    var selected_pplID : String = ""
    var message : String = ""
    val  vendorNameList = ArrayList<String>()
    val  vendorList = HashMap<String,Int>()
    val  peopleNameList = ArrayList<String>()
    val peopleHashList = HashMap<String,Int>()

    lateinit var viewModel: HomeViewModel
    var list: MutableList<HomeViewModelResponse> = ArrayList()
    var userDetails: LoginResponseModel? = null
    lateinit var assignworkReq : AssignProductRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_assignproduct)

        trans_id = intent.getIntExtra("transid",0)
        prodname = intent.getStringExtra("prodname").toString()
        //vendoregrouplist = intent.getSerializableExtra("groupdata") as ArrayList<Resultdata>
        (application as ApplicationController).applicationComponent.inject(this)

        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        //fetch group list
        viewModel.response().observe(this, { this.consumeGroupResponse(it as ApiResponse<*>) })
        fetchgroupapiCall()
        //pass vendor grp ID and fetch people list
        viewModel.response().observe(this, { this.consumePeopleResponse(it as ApiResponse<*>) })
        //assign work request
        viewModel.getAssignWorkResponse().observe(this, { this.consumeAssignWorkResponse(it as ApiResponse<*>) })

        binding.prodtitleVal.text = prodname

        binding.assignBtn.setOnClickListener(){
            assignworkReq = AssignProductRequest(
                transactionId = trans_id.toString(),
                groupId = selected_grpID,
                userid = selected_pplID
            )
            assignWorkapiCall(assignworkReq);
        }

    }

    private fun assignWorkapiCall(assignProductRequest: AssignProductRequest) {
        try {
            userDetails = sessionStore!!.getUserDetails()

            val e = Log.e(BaseActivity.TAG, "requestbodyHome - " + userDetails)

            viewModel.AssignDailyWork(assignProductRequest, ("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun consumeAssignWorkResponse(response: ApiResponse<*>) {
        Log.e(AllGroupofPeople.TAG, "consumePeopleResponse()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(AllGroupofPeople.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is AssignProductResponse) {
//                                sessionStore!!.saveUserDetails(it)
                                message = it.message.toString()
                                Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
                                finish()


//                                requireActivity().finish()
//                                it.let {
//                                    if (StringUtils.equalsIgnoreCase(it.token?.isEmpty().toString(), "")) {
//                            //                                        ContainerActivity.gotoContainerActivity(
//                            //                                            activity!!,
//                            //                                            getString(R.string.search)
//                            //                                        )
//                                        requireActivity().finish()
//                                    } else {
//                                        utility!!.showToast(
//                                            requireActivity(),
//                                            getString(R.string.invalidUser)
//                                        )
//                                    }
//
//                                }
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        /*val apiError =
                            errorUtils!!.parseError((response.error as HttpException).response())*/
                        //Log.e(TAG, "consumeResponse() apiError - ${apiError!!.error_description}")
                        //(response.error as HttpException).response().errorBody()
                        Log.e(AllGroupofPeople.TAG, "consumeResponse()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(AllGroupofPeople.TAG, "consumeResponseone() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(AllGroupofPeople.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }

    private fun fetchpeopleapiCall(groupID: String?) {
        try {
            userDetails = sessionStore!!.getUserDetails()
            val e = Log.e(BaseActivity.TAG, "requestbodyHome - " + userDetails)
            viewModel.grouppeopleistrequest(groupID, ("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun consumePeopleResponse(response: ApiResponse<*>) {
        Log.e(AllGroupofPeople.TAG, "consumePeopleResponse()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(AllGroupofPeople.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is GroupOfPeopleModel) {
//                                sessionStore!!.saveUserDetails(it)
                                peoplelist.clear()
                                peopleNameList.clear()
                                peopleHashList.clear()
                                peoplelist.addAll(it.result)
                                for(i in peoplelist.indices){
                                    //vendorIDlist.add(vendoregrouplist[i].id!!)
                                    peopleNameList.add(peoplelist[i].name!!)
                                    peopleHashList.put(peoplelist[i].name!!,peoplelist[i].id!!)
                                    updatedater(peopleNameList)
                                }

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        Log.e(AllGroupofPeople.TAG, "consumeResponse()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(AllGroupofPeople.TAG, "consumeResponseone() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(AllGroupofPeople.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }

    private fun updatedater(peopleNameList1: ArrayList<String>) {
        //People List spinner
        val ppladapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item, peopleNameList1)
        binding.volunteerVal.adapter = ppladapter

        binding.volunteerVal.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>,
                                        view: View, position: Int, id: Long) {
                selected_pplname = peopleNameList1[position]
                selected_pplID = peopleHashList.get(selected_pplname).toString()
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }


    }

    private fun fetchgroupapiCall() {
        try {
            userDetails = sessionStore!!.getUserDetails()
            val e = Log.e(BaseActivity.TAG, "requestbodyHome - " + userDetails)
            viewModel.vendorelistrequest(userDetails!!.message?.id, ("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun consumeGroupResponse(response: ApiResponse<*>) {
        Log.e(HomeFragment.TAG, "consumeGroupResponse()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(HomeFragment.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is HomeViewModelResponse) {
//                                sessionStore!!.saveUserDetails(it)
                                vendoregrouplist.addAll(it.result)

                                for(i in vendoregrouplist.indices){
                                    //vendorIDlist.add(vendoregrouplist[i].id!!)
                                    vendorNameList.add(vendoregrouplist[i].group_name!!)
                                    vendorList.put(vendoregrouplist[i].group_name!!,vendoregrouplist[i].id!!)
                                    updateAdapter(vendorNameList)
                                }

                             }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        Log.e(HomeFragment.TAG, "consumeResponse()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(HomeFragment.TAG, "consumeResponseone() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeFragment.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }

    private fun updateAdapter(vendorNameList1: ArrayList<String>) {
        //Group name spinner
        val adapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item, vendorNameList1
        )
        binding.grptitlevalue.adapter = adapter

        binding.grptitlevalue.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>,
                                        view: View, position: Int, id: Long) {

//                Toast.makeText(applicationContext,
//                    "item checked" + " " +
//                            "" + vendorNameList1[position], Toast.LENGTH_SHORT).show()
//                selected_grpname = grptitlevalue.getItemAtPosition(position) as String
                selected_grpname = vendorNameList1[position]
//                selected_grpname = parent.getItemAtPosition(position).toString()
                selected_grpID = vendorList.get(selected_grpname).toString()
 //               Log.e("groupidselect" , selected_grpID)
                fetchpeopleapiCall(selected_grpID);
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }
    }



}