package com.absyz.ssl.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.ssl.R
import com.absyz.ssl.databinding.ActivityTransactionBinding
import com.absyz.ssl.model.*
import com.absyz.ssl.viewmodel.HomeViewModel
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import fragment.AllGroupofPeople

class TransactionActivity: BaseActivity() {

    lateinit var binding: ActivityTransactionBinding

    var trans_id: Int =0
    var totqty: Int = 0
    var totamt : Int = 0
    var prodtitle : String = ""
    var message : String = ""
    val  userNameList = ArrayList<String>()
    val  userList = HashMap<String,Int>()
    var selectedUserName : String = ""
    var selectedUserID : String = ""
    var entered_qty : Int = 0
    var entered_amt : Int = 0


    lateinit var viewModel: HomeViewModel
    var userDetails: LoginResponseModel? = null
    private var productWiseUserlist: ArrayList<ProductWiseResult> = ArrayList()
    lateinit var transactionRequest : TransactionRequestModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_transaction)

        try {
            trans_id = intent.getIntExtra("transid",1)
            totqty = intent.getIntExtra("totqty",1)
            totamt = intent.getIntExtra("totamt",1)
            prodtitle = intent.getStringExtra("prodtitle").toString()

            binding.prodtitleVal.text = prodtitle
            binding.maxqty.text = totqty.toString()
            //binding.maxamt.text = totamt.toString()

            (application as ApplicationController).applicationComponent.inject(this)
            viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

            //fetch User list
            viewModel.productwiseDataRequest()?.observe(this, { this.consumeProductWiseUserResponse(it as ApiResponse<*>) })
            fetchpeoplewiseuserapiCall(trans_id.toString())
            //Send Transaction
            viewModel.transactionDataRequest()?.observe(this, { this.consumeTransactionResponse(it as ApiResponse<*>) })

            //entered_qty = binding.qtyVal.text.toString().toInt()
            //entered_amt = binding.amttitleVal.text.toString().toInt()

            binding.sendtrans.setOnClickListener {
                //Toast.makeText(this,"Submit", Toast.LENGTH_SHORT).show()
                transactionRequest = TransactionRequestModel(
                    transactionid = trans_id.toString(),
                    quantity = binding.qtyVal.text.toString(),
                    vendorid = userDetails!!.message?.id.toString(),
                    volunteerId = selectedUserID,
                    amount = binding.amttitleVal.text.toString()
                )
                transactionapiCall(transactionRequest)
                //validateFields(entered_qty,entered_amt)
            }

        } catch (ex: Exception){
            ex.printStackTrace();
        }

    }

    fun validateFields(qty : Int, amt: Int){
//        val entered_Quantity = qty.toString()
//        val entered_Amount = amt.toString()
//
//        if(qty<= totqty ){
////            transactionRequest = TransactionRequestModel(
////                transactionid = trans_id.toString(),
////                quantity = entered_Quantity,
////                vendorid = userDetails!!.message?.id.toString(),
////                volunteerId = selectedUserID,
////                amount = entered_Amount
////            )
////            transactionapiCall(transactionRequest)
//        } else {
//            //binding.qtyVal.setError("Please Enter Valid Data")
//        }
//
////        if(amt <= totamt ){
////            transactionRequest = TransactionRequestModel(
////                transactionid = trans_id.toString(),
////                quantity = entered_Quantity,
////                vendorid = userDetails!!.message?.id.toString(),
////                volunteerId = selectedUserID,
////                amount = entered_Amount
////            )
////            transactionapiCall(transactionRequest)
////        } else {
////            binding.amttitleVal.setError("Please Enter Valid Data")
////        }

    }

    private fun transactionapiCall(transRequest : TransactionRequestModel) {
        try {
            userDetails = sessionStore!!.getUserDetails()
            val e = Log.e(BaseActivity.TAG, "requestbodyHome - " + userDetails)
            viewModel.transactionDataResponse(transRequest, ("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    private fun fetchpeoplewiseuserapiCall(transID : String) {
        try {
            userDetails = sessionStore!!.getUserDetails()

            val e = Log.e(BaseActivity.TAG, "requestbodyHome - " + userDetails)

            viewModel.productwiseDataResponse(transID, ("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun consumeProductWiseUserResponse(response: ApiResponse<*>) {
        Log.e(AllGroupofPeople.TAG, "consumeProductWiseUserResponse()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(AllGroupofPeople.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is ProductWiseUserListResponse) {
//                                sessionStore!!.saveUserDetails(it)
                                message = it.message.toString()
                                Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
                                productWiseUserlist.addAll(it.result)
                                for(i in productWiseUserlist.indices){
                                    //vendorIDlist.add(vendoregrouplist[i].id!!)
                                    userNameList.add(productWiseUserlist[i].name!!)
                                    userList.put(productWiseUserlist[i].name!!,productWiseUserlist[i].volunteerId!!)
                                    setUserData(userNameList)
                                }

//                                requireActivity().finish()
//                                it.let {
//                                    if (StringUtils.equalsIgnoreCase(it.token?.isEmpty().toString(), "")) {
//                            //                                        ContainerActivity.gotoContainerActivity(
//                            //                                            activity!!,
//                            //                                            getString(R.string.search)
//                            //                                        )
//                                        requireActivity().finish()
//                                    } else {
//                                        utility!!.showToast(
//                                            requireActivity(),
//                                            getString(R.string.invalidUser)
//                                        )
//                                    }
//
//                                }
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        /*val apiError =
                            errorUtils!!.parseError((response.error as HttpException).response())*/
                        //Log.e(TAG, "consumeResponse() apiError - ${apiError!!.error_description}")
                        //(response.error as HttpException).response().errorBody()
                        Log.e(AllGroupofPeople.TAG, "consumeResponse()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(AllGroupofPeople.TAG, "consumeResponseone() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(AllGroupofPeople.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }

    fun setUserData(userNameList : ArrayList<String>){

        //User name spinner
        val adapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item, userNameList
        )
        binding.assiengeduserVal.adapter = adapter

        binding.assiengeduserVal.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>,
                                        view: View, position: Int, id: Long) {
                selectedUserName = userNameList[position]
//                selected_grpname = parent.getItemAtPosition(position).toString()
                selectedUserID = userList.get(selectedUserName).toString()
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }

    }

    private fun consumeTransactionResponse(response: ApiResponse<*>) {
        Log.e(AllGroupofPeople.TAG, "consumeTransactionResponse()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(AllGroupofPeople.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is TransactionResponseModel) {
//                                sessionStore!!.saveUserDetails(it)
                                message = it.message.toString()
                                Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
                                finish()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        /*val apiError =
                            errorUtils!!.parseError((response.error as HttpException).response())*/
                        //Log.e(TAG, "consumeResponse() apiError - ${apiError!!.error_description}")
                        //(response.error as HttpException).response().errorBody()
                        Log.e(AllGroupofPeople.TAG, "consumeResponse()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(AllGroupofPeople.TAG, "consumeResponseone() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(AllGroupofPeople.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }

}