package com.absyz.ssl.activities

import android.os.Bundle
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.absyz.ssl.R

class SplashActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)

        val image = findViewById<ImageView>(R.id.icon_img)

//        val animZoomIn: Animation =
//            AnimationUtils.loadAnimation(applicationContext, R.anim.zoom_in)
//        image.startAnimation(animZoomIn)
//
//        utility!!.applyDecimalPattern()

        Handler().postDelayed({
            //ScanActivity.gotoScanActivity(this)
            val bundle = Bundle()
            ContainerActivity.gotoContainerActivity(this, getString(R.string.welcome), bundle )

//            if (sessionStore!!.getUserDetails() == null){
//                ContainerActivity.gotoContainerActivity(this, getString(R.string.welcome), bundle )
//            } else {
//                ContainerActivity.gotoContainerActivity(this, getString(R.string.home), bundle)
//            }
            finish()
        }, 3000)

        //Rotate img animation
        val clk_rotate = AnimationUtils.loadAnimation(
            this,
            R.anim.rotate_clockwise
        )
        image.startAnimation(clk_rotate)

    }

}