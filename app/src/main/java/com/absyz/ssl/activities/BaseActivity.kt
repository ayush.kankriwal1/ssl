package com.absyz.ssl.activities

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.ssl.dagger.factory.ViewModelFactory
import com.checkin.helpers.InputValidator
import com.checkin.helpers.SessionStore
import com.checkin.helpers.Utility
import com.checkin.network.ErrorUtils
import javax.inject.Inject


abstract class BaseActivity : AppCompatActivity() {

    var dialog: Dialog? = null

    @set:Inject
    var viewModelFactory: ViewModelFactory? = null
//
//    @set:Inject
//    var pickMediaActivity: PickMediaActivity? = null

    @set:Inject
    var utility: Utility? = null

    @set:Inject
    var sessionStore: SessionStore? = null

    @set:Inject
    var inputValidator: InputValidator? = null

    @set:Inject
    var errorUtils: ErrorUtils? = null

    companion object {
        const val TAG = "BaseActivity"
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
//            (application as ApplicationController).applicationComponent!!.inject(this)

//            dialog = utility!!.getProgressDialogCircle(this)

        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "onCreate() Exception - ${ex.message}")
         }

    }

    open fun showLoader() {
        try {
            Log.e(TAG, "showLoader()")
            runOnUiThread {
                try {
                    if (dialog != null) {
                        dialog!!.show()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
            Log.e(TAG, "showLoader() Exception - " + ex.message)
        }
    }

    override fun onBackPressed() {
        try {
            if (supportFragmentManager.backStackEntryCount > 1) {
                supportFragmentManager.popBackStack()
            } else {
                super.onBackPressed()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    open fun dismissLoader() {
        try {
            Log.e(TAG, "dismissLoader()")
            runOnUiThread {
                try {
                    if (dialog != null) {
                        dialog!!.dismiss()
                    }
                 } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
            Log.e(TAG, "showLoader() Exception - " + ex.message)
        }
    }


}










//import android.app.Dialog
//import android.os.Bundle
//import android.util.Log
//import androidx.appcompat.app.AppCompatActivity
//import com.absyz.kotlin.dagger.ApplicationController
//import com.absyz.kotlin.dagger.factory.ViewModelFactory
//import com.checkin.helpers.SessionStore
//import com.checkin.helpers.Utility
//import com.checkin.network.ErrorUtils
//import javax.inject.Inject
//
//abstract class BaseActivity : AppCompatActivity() {
//
//    var dialog: Dialog? = null
//
//    @set:Inject
//    var viewModelFactory: ViewModelFactory? = null
//
////    @set:Inject
////    var pickMediaActivity: PickMediaActivity? = null
//
//    @set:Inject
//    var utility: Utility? = null
//
//    @set:Inject
//    var sessionStore: SessionStore? = null
//
////    @set:Inject
////    var inputValidator: InputValidator? = null
//
//    @set:Inject
//    var errorUtils: ErrorUtils? = null
//
//    companion object {
//        const val TAG = "BaseActivity"
//    }
//
//    public override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        try {
//            (application as ApplicationController).applicationComponent!!.inject(this)
//
////            dialog = utility!!.getProgressDialogCircle(this)
//
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//            Log.e(TAG, "onCreate() Exception - ${ex.message}")
//        }
//
//    }
//
//    open fun showLoader() {
//        try {
//            Log.e(TAG, "showLoader()")
//            runOnUiThread {
//                try {
//                    if (dialog != null) {
//                        dialog!!.show()
//                    }
//                } catch (e: java.lang.Exception) {
//                    e.printStackTrace()
//                }
//            }
//        } catch (ex: java.lang.Exception) {
//            ex.printStackTrace()
//            Log.e(TAG, "showLoader() Exception - " + ex.message)
//        }
//    }
//
//    open fun dismissLoader() {
//        try {
//            Log.e(TAG, "dismissLoader()")
//            runOnUiThread {
//                try {
//                    if (dialog != null) {
//                        dialog!!.dismiss()
//                    }
//                } catch (e: java.lang.Exception) {
//                    e.printStackTrace()
//                }
//            }
//        } catch (ex: java.lang.Exception) {
//            ex.printStackTrace()
//            Log.e(TAG, "showLoader() Exception - " + ex.message)
//        }
//    }
//
//}
//
//
//
//
//
//
//
//
//
//
