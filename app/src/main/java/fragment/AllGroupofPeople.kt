package fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.ssl.R
import com.absyz.ssl.activities.BaseActivity
import com.absyz.ssl.activities.ContainerActivity
import com.absyz.ssl.activities.EditPeopleActivity
import com.absyz.ssl.activities.EditVendorActivity
import com.absyz.ssl.adapter.GroupOfPeopleAdapter
import com.absyz.ssl.databinding.FragmentAllgroupofpeopleBinding
import com.absyz.ssl.model.*
import com.absyz.ssl.viewmodel.HomeViewModel
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus

class AllGroupofPeople(grpId : String) : BaseFragment(), GroupOfPeopleAdapter.ItemClickListener{

    lateinit var binding: FragmentAllgroupofpeopleBinding
    lateinit var viewModel: HomeViewModel
    var userDetails: LoginResponseModel? = null
    lateinit var GroupofPeopleadapter: GroupOfPeopleAdapter
    var GroupID = grpId
    //var GroupID : String= "2"

    companion object {
        const val TAG = "AllGroupofPeople"
        //@JvmStatic val fragment = AllGroupofPeople()
    }

    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragment_allgroupofpeople, parent, false)
        return binding.root
    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        arguments?.let {
//
//        }
//    }


//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//
//        try{
//            binding.groupid.text = GroupID
//
//        }catch (ex: Exception){
//            ex.printStackTrace()
//        }
//    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        try {
            viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

            viewModel.response()
                .observe(viewLifecycleOwner, { this.  consumeResponse(it as ApiResponse<*>) })

            binding.groupnameid.text = "People Available in the Group:- " +GroupID as String


            apiCall(GroupID);

            binding.addpeople.setOnClickListener{
                val bundle = Bundle()
                bundle.putString("id", GroupID);
                Log.e(BaseActivity.TAG, "groupID - " + GroupID)
                ContainerActivity.gotoContainerActivity(
                    requireActivity(),
                    getString(R.string.addpeople),
                    bundle
                )
            }


            // setSupportActionBar method
//            (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun apiCall(groupID: String?) {
        try {
            userDetails = sessionStore!!.getUserDetails()

            val e = Log.e(BaseActivity.TAG, "requestbodyHome - " + userDetails)

            viewModel.grouppeopleistrequest(groupID, ("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun consumeResponse(response: ApiResponse<*>) {
        Log.e(AllGroupofPeople.TAG, "consumeResponseHome()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(AllGroupofPeople.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is GroupOfPeopleModel) {
//                                sessionStore!!.saveUserDetails(it)

                                setAdapter(it);



//                                requireActivity().finish()
//                                it.let {
//                                    if (StringUtils.equalsIgnoreCase(it.token?.isEmpty().toString(), "")) {
//                            //                                        ContainerActivity.gotoContainerActivity(
//                            //                                            activity!!,
//                            //                                            getString(R.string.search)
//                            //                                        )
//                                        requireActivity().finish()
//                                    } else {
//                                        utility!!.showToast(
//                                            requireActivity(),
//                                            getString(R.string.invalidUser)
//                                        )
//                                    }
//
//                                }
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        /*val apiError =
                            errorUtils!!.parseError((response.error as HttpException).response())*/
                        //Log.e(TAG, "consumeResponse() apiError - ${apiError!!.error_description}")
                        //(response.error as HttpException).response().errorBody()
                        Log.e(AllGroupofPeople.TAG, "consumeResponse()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(AllGroupofPeople.TAG, "consumeResponseone() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(AllGroupofPeople.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }

    private fun setAdapter(any: GroupOfPeopleModel) {
        try {
//            list.addAll(listOf(any))
            GroupofPeopleadapter= GroupOfPeopleAdapter(requireContext(), this, utility!!)
            utility!!.getRecyclerview(
                requireContext(),
                binding.groupofpeoplelist,
                false,

                isDivider = true
            )!!.adapter = GroupofPeopleadapter

            //sampleJson()
            GroupofPeopleadapter.updateList(any.result)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onItemClick(item: ResultGroup) {
        val intent_assignproduct = Intent(context, EditPeopleActivity::class.java)
        intent_assignproduct.putExtra("id",item.id)
        intent_assignproduct.putExtra("grpid",item.groupId)
        intent_assignproduct.putExtra("adharno",item.adharNo)
        intent_assignproduct.putExtra("name",item.name)
        intent_assignproduct.putExtra("status",item.status)
        intent_assignproduct.putExtra("profpic",item.profilePic)
        intent_assignproduct.putExtra("proofpic",item.proofPic)

        startActivity(intent_assignproduct)
    }

    override fun onResume() {
        super.onResume()
        apiCall(GroupID);
    }

}