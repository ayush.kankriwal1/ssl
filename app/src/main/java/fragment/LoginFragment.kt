package fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.ssl.R
import com.absyz.ssl.activities.BaseActivity
import com.absyz.ssl.activities.ContainerActivity
import com.absyz.ssl.databinding.FragmentLoginBinding
import com.absyz.ssl.model.LoginResponseModel
import com.absyz.ssl.model.Loginmodel
import com.absyz.ssl.viewmodel.SignUpViewModel
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus


class LoginFragment : BaseFragment(), TextWatcher {

    lateinit var binding: FragmentLoginBinding

    private var editTextList: MutableList<EditText> = java.util.ArrayList()
    lateinit var validateRequest: Loginmodel

    lateinit var viewModel: SignUpViewModel

    companion object {
        const val token = ""
        const val TAG = "LoginFragment"
    }

    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragment_login, parent, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        try {

            addListener()

            //binding.etUserName.setText("Mythili")
            //binding.etPassword.setText("Password@1")

            //Rotate img animation
            val image = binding.iconImg
            val clk_rotate = AnimationUtils.loadAnimation(
                context,
                R.anim.rotate_clockwise
            )
            image.startAnimation(clk_rotate)

            binding.tvLogin.setOnClickListener {
                isValidData()
            }
//            binding.checkbox.setOnClickListener {
//                val sceneViewerIntent = Intent(Intent.ACTION_VIEW)
//                sceneViewerIntent.data = Uri.parse("https://arvr.google.com/scene-viewer/1.0?file=https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/Avocado/glTF/Avocado.gltf")
//                sceneViewerIntent.setPackage("com.google.android.googlequicksearchbox")
//                startActivity(sceneViewerIntent)
//            }

            viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(SignUpViewModel::class.java)

            viewModel.response()
                .observe(viewLifecycleOwner, { this.  consumeResponse(it as ApiResponse<*>) })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun addListener() {
        try {


            editTextList.add(binding.etUserName)
            editTextList.add(binding.etPassword)

            binding.etUserName.addTextChangedListener(this)
            binding.etPassword.addTextChangedListener(this)


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun isValidData(): Boolean {
        var isValid = false
        try {
            if (!utility!!.hasEmptyError(editTextList)) {
                 validateRequest = Loginmodel(
                    username = binding.etUserName.text.toString().trim(),
                    password = binding.etPassword.text.toString().trim()
                )
                Log.e(BaseActivity.TAG, "requestbody - " + validateRequest)
                viewModel.validateUser(validateRequest)
                isValid = true
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return isValid
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
        try {
            utility!!.setError(binding.tvError, "")
            when {
                binding.etUserName.text!!.hashCode() == s.hashCode() -> {
                    utility!!.editTextBackgroundTextChange(binding.etUserName)
                }
                binding.etPassword.text!!.hashCode() == s.hashCode() -> {
                    utility!!.editTextBackgroundTextChange(binding.etPassword)
                }
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    override fun afterTextChanged(p0: Editable?) {

    }

    private fun consumeResponse(response: ApiResponse<*>) {
        Log.e(TAG, "consumeResponse()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(TAG, "responsedata ${apiResponse.data}")
                            if (it is LoginResponseModel) {
                                sessionStore!!.saveUserDetails(it)

                                val bundle = Bundle()
                                bundle.putString("id", it.userId);
                                bundle.putString("token", it.token);


                                ContainerActivity.gotoContainerActivity(
                                    requireActivity(),
                                    getString(R.string.home),
                                    bundle

                                )
//                                requireActivity().finish()
//                                it.let {
//                                    if (StringUtils.equalsIgnoreCase(it.token?.isEmpty().toString(), "")) {
//                            //                                        ContainerActivity.gotoContainerActivity(
//                            //                                            activity!!,
//                            //                                            getString(R.string.search)
//                            //                                        )
//                                        requireActivity().finish()
//                                    } else {
//                                        utility!!.showToast(
//                                            requireActivity(),
//                                            getString(R.string.invalidUser)
//                                        )
//                                    }
//
//                                }
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        /*val apiError =
                            errorUtils!!.parseError((response.error as HttpException).response())*/
                        //Log.e(TAG, "consumeResponse() apiError - ${apiError!!.error_description}")
                        //(response.error as HttpException).response().errorBody()
                        utility!!.setError(binding.tvError, response.error!!.message!!)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(TAG, "consumeResponse() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }
}