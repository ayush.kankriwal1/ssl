package fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.ssl.R
import com.absyz.ssl.activities.BaseActivity
import com.absyz.ssl.adapter.WorkAssignedGroupOfPeopleAdapter
import com.absyz.ssl.databinding.FragmentAssignedgroupofpeopleBinding
import com.absyz.ssl.model.*
import com.absyz.ssl.viewmodel.HomeViewModel
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus

class AssignedGroupofPeople(grpId : String) : BaseFragment(), WorkAssignedGroupOfPeopleAdapter.ItemClickListener{

    lateinit var binding: FragmentAssignedgroupofpeopleBinding
    lateinit var viewModel: HomeViewModel
    var userDetails: LoginResponseModel? = null
    lateinit var workAssignedAdapter: WorkAssignedGroupOfPeopleAdapter
    //var GroupID: String? = "2"
    var VendorID : String? = ""
    var GroupID = grpId
    var selectedUserId : String = ""
    lateinit var message : String
    private var assigneduserDetials : ArrayList<WorkAssignedUserModelResponse> = ArrayList()

    companion object {
        const val TAG = "AssignedGroupofPeople"
    }


    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragment_assignedgroupofpeople, parent, false)
        return binding.root

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        try {
            viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

            viewModel.getworkassigneduserResponse()
                ?.observe(viewLifecycleOwner, { this.  consumeWorkAssignedUserResponse(it as ApiResponse<*>) })
            apiCall(GroupID!!);

            viewModel.unassignUserRequest()
                ?.observe(viewLifecycleOwner, { this.  consumeUnassignUserResponse(it as ApiResponse<*>) })

            // setSupportActionBar method
//            (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun apiCall(grpId: String) {
        try {
            userDetails = sessionStore!!.getUserDetails()
            VendorID = userDetails!!.message?.id!!.toString()

            val e = Log.e(BaseActivity.TAG, "assigned group of people - " + userDetails)
            //Toast.makeText(context,"grpId: " + grpId + "vendorID: "+userDetails!!.message?.id!!, Toast.LENGTH_SHORT).show()
            viewModel.workassigneduserrequest(grpId, userDetails!!.message?.id!!,("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun consumeWorkAssignedUserResponse(response: ApiResponse<*>) {
        Log.e(AllGroupofPeople.TAG, "consumeWorkAssignedUserResponse()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(AssignedGroupofPeople.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is WorkAssignedUserModelResponse) {
//                                sessionStore!!.saveUserDetails(it)
                                Log.d("work Assign:", it.result.toString())
                                //assigneduserDetials.addAll(it.result)
                                setAdapter(it);

//                                requireActivity().finish()
//                                it.let {
//                                    if (StringUtils.equalsIgnoreCase(it.token?.isEmpty().toString(), "")) {
//                            //                                        ContainerActivity.gotoContainerActivity(
//                            //                                            activity!!,
//                            //                                            getString(R.string.search)
//                            //                                        )
//                                        requireActivity().finish()
//                                    } else {
//                                        utility!!.showToast(
//                                            requireActivity(),
//                                            getString(R.string.invalidUser)
//                                        )
//                                    }
//
//                                }
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        /*val apiError =
                            errorUtils!!.parseError((response.error as HttpException).response())*/
                        //Log.e(TAG, "consumeResponse() apiError - ${apiError!!.error_description}")
                        //(response.error as HttpException).response().errorBody()
                        Log.e(AllGroupofPeople.TAG, "consumeResponse()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(AllGroupofPeople.TAG, "consumeResponseone() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(AllGroupofPeople.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }

    private fun setAdapter(any: WorkAssignedUserModelResponse) {
        try {
//            list.addAll(listOf(any))

            if(any.result.isEmpty()){
                binding.nodata.visibility = View.VISIBLE
                binding.groupofpeoplelist.visibility = View.GONE
            } else {
                binding.nodata.visibility = View.GONE
                binding.groupofpeoplelist.visibility = View.VISIBLE
                workAssignedAdapter= WorkAssignedGroupOfPeopleAdapter(requireContext(),this, utility!!)
                utility!!.getRecyclerview(
                    requireContext(),
                    binding.groupofpeoplelist,
                    false,
                    isDivider = true
                )!!.adapter = workAssignedAdapter

                //sampleJson()
                //Toast.makeText(context,"result : "+ any.result, Toast.LENGTH_SHORT).show()
                workAssignedAdapter.updateList(any.result)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun unassignUserapiCall(userId : String) {
        try {
            userDetails = sessionStore!!.getUserDetails()

            val e = Log.e(BaseActivity.TAG, "Unassign User- " + userDetails)
            //Toast.makeText(context,"vendorID: "+userDetails!!.message?.id!!, Toast.LENGTH_SHORT).show()
            viewModel.unassignUserResponse(userId,("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun consumeUnassignUserResponse(response: ApiResponse<*>) {
        Log.e(AllGroupofPeople.TAG, "consumeUnassignUserResponse()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is UnassignResponseModel) {
//                                sessionStore!!.saveUserDetails(it)
                                message = it.message.toString()
                                Toast.makeText(context,message, Toast.LENGTH_SHORT).show()
                                apiCall(GroupID!!);
                                //assigneduserDetials.addAll(it.result)
                                //setUnssignedAdapter(it)

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        Log.e("Unassigned User", "UnassignResponse()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e("Unassigned User", "UnassignResponse() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e("Unassigned User", "UnassignResponse() Exceptionmanin - ${ex.message}")
        }

    }

    override fun onItemClick(item:WorkAssignedUserResult) {
        //Call unassign api
        //Toast.makeText(context,"Work Unassigned", Toast.LENGTH_SHORT).show()
        selectedUserId = item.id.toString()
        unassignUserapiCall(selectedUserId)
        //apiCall(GroupID!!);
    }

}