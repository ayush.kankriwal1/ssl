package fragment

import android.app.Dialog
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.ssl.activities.BaseActivity
import com.absyz.ssl.dagger.factory.ViewModelFactory
import com.absyz.ssl.model.LoginResponseModel
import com.checkin.helpers.InputValidator
import com.checkin.helpers.SessionStore
import com.checkin.helpers.Utility
import com.checkin.network.ErrorUtils
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import javax.inject.Inject

abstract class BaseFragment : Fragment() {

    var dialog: Dialog? = null
//
//    @set:Inject
//    var pickMediaActivity: PickMediaActivity? = null

    @set:Inject
    var utility: Utility? = null

    @set:Inject
    var sessionStore: SessionStore? = null

    @set:Inject
    var inputValidator: InputValidator? = null

    @set:Inject
    var viewModelFactory: ViewModelFactory? = null

    @set:Inject
    var errorUtils: ErrorUtils? = null

    companion object {
        const val TAG = "BaseFragment"

            private val IMAGE_DIRECTORY = "/demonuts"

    }

    abstract fun provideFragmentView(
        inflater: LayoutInflater?,
        parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?

    override fun onCreateView(
        inflater: LayoutInflater,
        parent: ViewGroup?,
        savedInstanseState: Bundle?): View? {
        Log.e(TAG,"onCreateView()")
        val rootView = provideFragmentView(inflater,parent,savedInstanseState)
        (requireActivity().application as ApplicationController).applicationComponent.inject(this)
        dialog = utility!!.getProgressDialogCircle(activity)
        return rootView
    }

    open fun onBackPressed() {}

    internal fun firstTimeCreated(savedInstanceState: Bundle?) = savedInstanceState == null

    open fun showLoader() {
        try {
            Log.e(TAG, "showLoader()")
            requireActivity().runOnUiThread {
                try {
                    if (dialog != null) {
                        dialog!!.show()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
            Log.e(BaseActivity.TAG, "showLoader() Exception - " + ex.message)
        }
    }

    open fun dismissLoader() {
        try {
            Log.e(TAG, "dismissLoader()")
            requireActivity().runOnUiThread {
                try {
                    if (dialog != null) {
                        dialog!!.dismiss()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
            Log.e(BaseActivity.TAG, "showLoader() Exception - " + ex.message)
        }
    }

    fun saveImage(myBitmap: Bitmap):String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY)
        // have the object build the directory structure, if needed.
        Log.d("fee",wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists())
        {

            wallpaperDirectory.mkdirs()
        }

        try
        {
            Log.d("heel",wallpaperDirectory.toString())
            val f = File(wallpaperDirectory, ((Calendar.getInstance()
                .getTimeInMillis()).toString() + ".jpg"))
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(requireContext(),
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null)
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        }
        catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }


}