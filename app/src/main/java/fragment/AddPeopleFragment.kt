package fragment

import android.Manifest
import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.ssl.R
import com.absyz.ssl.activities.ContainerActivity
import com.absyz.ssl.cameraview.PicModeSelectDialogFragment
import com.absyz.ssl.cameraview.UtilityJava
import com.absyz.ssl.databinding.FragmentaddPeopleBinding
import com.absyz.ssl.helpers.RealPathUtil.getRealPath
import com.absyz.ssl.helpers.URIPathHelper
import com.absyz.ssl.model.Addpeopleingroup
import com.absyz.ssl.model.ResultUpload
import com.absyz.ssl.model.UploadResponse
import com.absyz.ssl.viewmodel.HomeViewModel
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.network.Repository
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class AddPeopleFragment : BaseFragment() , TextWatcher ,
    PicModeSelectDialogFragment.IPicModeSelectListener {
    val REQUEST_CODE_UPDATE_PIC = 0x1
    lateinit var mPhotoFile: String
    private val REQUEST_PERMISSION = 100
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_PICK_IMAGE = 2

    var boolrequest :  Boolean = false
    var Document_img1: String = ""
    lateinit var binding: FragmentaddPeopleBinding
    private var editTextList: MutableList<EditText> = java.util.ArrayList()
    var cal = Calendar.getInstance()
    lateinit var viewModel: HomeViewModel
    var GroupID: String? = ""
    var volunteerName : String? = ""
    var bundle = Bundle()
    lateinit var filePath: File
    private val repository: Repository? = null

    private var uploadResult: ArrayList<ResultUpload> = ArrayList()
    var profilePic : String = ""
    var proofPic : String = ""
    var message : String = ""

    //Our variables
    private var mUri: Uri? = null

    //Our constants
    private val GALLERY = 1
    private val CAMERA = 2


    companion object {
        const val TAG = "AddpeopleGroup"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            GroupID = it.getString("id")
        }
    }

    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragmentadd_people, parent, false)
        return binding.root

    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        try{

            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_IMAGE_CAPTURE) {
                    val bitmap = data?.extras?.get("data") as Bitmap
                    val uri = getImageUri(requireContext(),bitmap);

                    if(boolrequest){
                        binding.uploadProofView.setImageBitmap(bitmap)
                        binding.uploadProofView.setImageURI(uri)

                        val realPathUtil=   getRealPath(requireActivity(), uri)
                        val file = File(realPathUtil);
                        val userDetails = sessionStore!!.getUserDetails()
//

                            viewModel.uploadProofPhoto(file, "absyz "+ userDetails!!.token)

                        //proofPic = uri.toString()
                    }else{
                        binding.uploadprofilpic.setImageBitmap(bitmap)
                        binding.uploadprofilpic.setImageURI(uri)
                        //profilePic = uri.toString()
//                        val realPathUtil=   getRealPath(requireContext(), uri );
//                        val file = File(realPathUtil);
                        val file = File(uri?.getPath())
                        val userDetails = sessionStore!!.getUserDetails()



//
                        viewModel.uploadProfilePhoto(file, "absyz "+ userDetails!!.token)
                    }

                }
                else if (requestCode == REQUEST_PICK_IMAGE) {
                    val uri = data!!.getData()
//                  filePath = data!!.data!!.toFile()
                    //Upload proof
                    //binding.uploadImageView.setImageURI(uri)

                    if(boolrequest){
                        binding.uploadProofView.setImageURI(uri)
                        //proofPic = uri.toString()

                        //                  getTakenPictureUri(context, uri)
                        val uriPathHelper = URIPathHelper()
                        val realPathUtil=   getRealPath(requireContext(), uri );
                        val file = File(realPathUtil);
                        Log.d("filepath", realPathUtil )
                        Log.d("filepath2", file.absolutePath )

//                  filePath = uriPathHelper.getPath(requireContext(), uri!!)!!
                        val requestFile: RequestBody =
                            RequestBody.create(MediaType.parse("multipart/form-data"), (file))

                        val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
                        val userDetails = sessionStore!!.getUserDetails()

                        viewModel.uploadProofPhoto(file, "absyz "+ userDetails!!.token)

                    }else{
                        binding.uploadprofilpic.setImageURI(uri)
                        //profilePic = uri.toString()

                        //                  getTakenPictureUri(context, uri)
                        val uriPathHelper = URIPathHelper()
                        val realPathUtil=   getRealPath(requireContext(), uri );
                        val file = File(realPathUtil);
                        Log.d("filepath", realPathUtil )
                        Log.d("filepath2", file.absolutePath )

//                  filePath = uriPathHelper.getPath(requireContext(), uri!!)!!
                        val requestFile: RequestBody =
                            RequestBody.create(MediaType.parse("multipart/form-data"), (file))

                        val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
                        val userDetails = sessionStore!!.getUserDetails()

                        viewModel.uploadProfilePhoto(file, "absyz "+ userDetails!!.token)
                    }



//        file = file.split("/")[file.split("/").length - 1];
                }
            }

        }catch (ex: Exception){
            ex.printStackTrace();
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        try {
            addListener()
            viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

            viewModel.response()
                .observe(viewLifecycleOwner, { this.consumeResponse(it as ApiResponse<*>) })

            viewModel.getuploadeproofview()
                ?.observe(viewLifecycleOwner, { this.  consumeProofResponse(it as ApiResponse<*>) })

            viewModel.getuploadeprofileview()
                ?.observe(viewLifecycleOwner, { this.  consumeProfileResponse(it as ApiResponse<*>) })

            //binding.etUserName.setText("Mythili")
            //binding.etPassword.setText("Password@1")

            binding.tvLogin.setOnClickListener {
                isValidData()
            }
            binding.uploadTxtview.setOnClickListener {
                boolrequest  = true
                showAddProfilePicDialog()
            }

            binding.uploadProofView.setOnClickListener {
                boolrequest  = true
                showAddProfilePicDialog()            }

            binding.uploadprofilpic.setOnClickListener {

                boolrequest = false
                showAddProfilePicDialog()            }

            val sdf = SimpleDateFormat("MM/dd/yyyy")
            val currentDate = sdf.format(Date())
            binding.edtdate.text = Editable.Factory.getInstance().newEditable(currentDate)
            binding.edtdate.setOnClickListener {
                DatePickerDialog(
                    requireContext(),
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)
                ).show()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun showAddProfilePicDialog() {
        val dialogFragment = PicModeSelectDialogFragment()
        dialogFragment.setiPicModeSelectListener(this)
        dialogFragment.show(requireActivity().getSupportFragmentManager(), "picModeSelector")

    }

    private fun choosePhotoFromGallary() {
        val galleryIntent = Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(galleryIntent, GALLERY)
    }

    // create an OnDateSetListener
    val dateSetListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(
            view: DatePicker, year: Int, monthOfYear: Int,
            dayOfMonth: Int
        ) {
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateDateInView()
        }
    }

    private fun updateDateInView() {
        val myFormat = "MM/dd/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        binding.edtdate.text =
            Editable.Factory.getInstance().newEditable(sdf.format(cal.getTime()))
    }

    fun isValidData(): Boolean {
        var isValid = false
        try {
            if (!utility!!.hasEmptyError(editTextList)) {
                val userDetails = sessionStore!!.getUserDetails()

                val adharnumber = binding.etAdhar.text.toString().trim()
                volunteerName = binding.etname.text.toString()

                viewModel.validateaddpeople(
                    adharnumber,
                    GroupID.toString(),
                    volunteerName!!,
                    profilePic,
                    proofPic,
                    ("absyz " + (userDetails?.token))
                )
                isValid = true
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return isValid
    }

    private fun addListener() {
        try {
            editTextList.add(binding.etname)
            editTextList.add(binding.etAdhar)
            editTextList.add(binding.edtdate)
//            editTextList.add(binding.edtgroupID)
//            editTextList.add(binding.edtvendoreid)

            binding.etname.addTextChangedListener(this)
            binding.etname.addTextChangedListener(this)
//            binding.edtgroupID.addTextChangedListener(this)
//            binding.edtvendoreid.addTextChangedListener(this)
            binding.etAdhar.addTextChangedListener(this)


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
        try {
            utility!!.setError(binding.tvError, "")
            when {
                binding.etname.text!!.hashCode() == s.hashCode() -> {
                    utility!!.editTextBackgroundTextChange(binding.etname)
                }
                binding.etAdhar.text!!.hashCode() == s.hashCode() -> {
                    utility!!.editTextBackgroundTextChange(binding.etAdhar)
                }
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }

    }

    override fun afterTextChanged(p0: Editable?) {}

    private fun consumeResponse(response: ApiResponse<*>) {
        Log.e(AllGroupofPeople.TAG, "consumeResponseHome()" + response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(AllGroupofPeople.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is Addpeopleingroup) {
//                                sessionStore!!.saveUserDetails(it)
//                                setAdapter(it);
                                ContainerActivity.gotoContainerActivity(
                                    requireActivity(),
                                    getString(R.string.home),
                                    bundle
                                )
//                                requireActivity().finish()
//                                it.let {
//                                    if (StringUtils.equalsIgnoreCase(it.token?.isEmpty().toString(), "")) {
//                            //                                        ContainerActivity.gotoContainerActivity(
//                            //                                            activity!!,
//                            //                                            getString(R.string.search)
//                            //                                        )
//                                        requireActivity().finish()
//                                    } else {
//                                        utility!!.showToast(
//                                            requireActivity(),
//                                            getString(R.string.invalidUser)
//                                        )
//                                    }
//
//                                }
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        /*val apiError =
                        errorUtils!!.parseError((response.error as HttpException).response())*/
                        //Log.e(TAG, "consumeResponse() apiError - ${apiError!!.error_description}")
                        //(response.error as HttpException).response().errorBody()
                        Log.e(
                            AllGroupofPeople.TAG,
                            "consumeResponse()data - ${response.error!!.cause!!}"
                        )

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(
                            AllGroupofPeople.TAG,
                            "consumeResponseone() Exception0 - ${ex.printStackTrace()}"
                        )
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(AllGroupofPeople.TAG, "consumeResponse() Exceptionmanin - ${ex.printStackTrace()}")
        }

    }

    private fun consumeProofResponse(response: ApiResponse<*>) {
        Log.e(AllGroupofPeople.TAG, "consumeResponseHome()$response")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(AllGroupofPeople.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is UploadResponse) {
//                                sessionStore!!.saveUserDetails(it)
                                Toast.makeText(context,"Message : "+ it.message, Toast.LENGTH_SHORT).show()
                                setProofAdapter(it.result);
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        Log.e(
                            AllGroupofPeople.TAG,
                            "consumeResponse()data - ${response.error!!.message!!}"
                        )

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(
                            AllGroupofPeople.TAG,
                            "consumeResponseone() Exception0 - ${ex.message}"
                        )
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(AllGroupofPeople.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }

    private fun setProofAdapter(uploadResult: ResultUpload?){
        proofPic = uploadResult?.imagePath.toString()
    }

    private fun consumeProfileResponse(response: ApiResponse<*>) {
        Log.e(AllGroupofPeople.TAG, "consumeResponseHome()$response")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(AllGroupofPeople.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is UploadResponse) {
//                                sessionStore!!.saveUserDetails(it)
                                Toast.makeText(context,"Message : "+ it.message, Toast.LENGTH_SHORT).show()
                                setProfileAdapter(it.result);
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        /*val apiError =
                        errorUtils!!.parseError((response.error as HttpException).response())*/
                        //Log.e(TAG, "consumeResponse() apiError - ${apiError!!.error_description}")
                        //(response.error as HttpException).response().errorBody()
                        Log.e(
                            AllGroupofPeople.TAG,
                            "consumeResponse()data - ${response.error!!.message!!}"
                        )

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(
                            AllGroupofPeople.TAG,
                            "consumeResponseone() Exception0 - ${ex.message}"
                        )
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(AllGroupofPeople.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }

    private fun setProfileAdapter(uploadResult: ResultUpload?){
        profilePic = uploadResult?.imagePath.toString()
    }

    override fun onPicModeSelected(mode: String?) {
        val action: String =
            if (mode.equals(UtilityJava.PicModes.CAMERA))

                openCamera()
            else
                openGallery()
    }

    private fun openGallery(): String {
        Intent(Intent.ACTION_GET_CONTENT).also { intent ->
            intent.type = "image/*"
            intent.resolveActivity(requireActivity().packageManager)?.also {
                startActivityForResult(intent, REQUEST_PICK_IMAGE)
            }
        }
        return String()

    }

    override fun onResume() {
        super.onResume()
        checkCameraPermission()
    }

    private fun checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.CAMERA),
                REQUEST_PERMISSION)
        }    }

    private fun openCamera(): String {

        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { intent ->
            intent.resolveActivity(requireActivity().packageManager)?.also {
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
            }
            return String()
        }
    }


}




