package fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.ssl.R
import com.absyz.ssl.activities.*
import com.absyz.ssl.adapter.ProductListAdapter
import com.absyz.ssl.adapter.VendorAdapter
import com.absyz.ssl.databinding.FragmentHomeBinding
import com.absyz.ssl.model.*
import com.absyz.ssl.viewmodel.HomeViewModel
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import java.text.SimpleDateFormat


class HomeFragment : BaseFragment(), ProductListAdapter.ItemClickListener {

    lateinit var binding: FragmentHomeBinding

    lateinit var viewModel: HomeViewModel
    var userDetails: LoginResponseModel? = null
    var list: MutableList<HomeViewModelResponse> = ArrayList()
    var listproduct: MutableList<ProductsAssigned> = ArrayList()
    lateinit var vendoreadapter: VendorAdapter
    lateinit var productadapter: ProductListAdapter
    private var producttoassignlist: ArrayList<ResultProduct> = ArrayList()
    private var vendorelist: ArrayList<Resultdata> = ArrayList()

    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

    var date : String = ""

    companion object {
        const val TAG = "HomeFragment"
    }


    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragment_home, parent, false)
        return binding.root

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        try {
            viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

            viewModel.response()
                .observe(viewLifecycleOwner, { this.  consumeResponse(it as ApiResponse<*>) })

            viewModel.getuserdetailsResponse()
                ?.observe(viewLifecycleOwner, { this.  consumeResponseProduct(it as ApiResponse<*>) })

            apiCall();
            apiCallproduct();

            // setSupportActionBar method
            //(activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun apiCallproduct() {
        try {

            userDetails = sessionStore!!.getUserDetails()

            val e = Log.e(BaseActivity.TAG, "requestbodyHome - " + userDetails)

            viewModel.productlistrequest(userDetails!!.message?.id, ("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }    }

    private fun setVendorAdapter() {
        try {

            //list.addAll(listOf(any))
            vendoreadapter= VendorAdapter(requireContext(),  utility!!)
            utility!!.getRecyclerview(
                requireContext(),
                binding.vendorListView,
                true,
                false
            )!!.adapter = vendoreadapter

            //sampleJson()
            vendoreadapter.updateList(vendorelist)
            vendoreadapter.setOnItemCLickListerner(object :VendorAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {

                    //Toast.makeText(context, wishListDetails[position].Name, Toast.LENGTH_SHORT).show()

                    val intent_grps = Intent(context, GroupsActivity::class.java)
                    intent_grps.putExtra("grpId",vendorelist[position].id)

                    startActivity(intent_grps)
                }
            } )


        } catch (ex: Exception) {
            ex.printStackTrace()
        }    }

    private fun apiCall() {
        try {

            userDetails = sessionStore!!.getUserDetails()

            val e = Log.e(BaseActivity.TAG, "requestbodyHome - " + userDetails)

            viewModel.vendorelistrequest(userDetails!!.message?.id, ("absyz "+(userDetails?.token)) )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

//
//    // This function is used to add items in arraylist and assign
//    // the adapter to view pager
//    private fun setupViewPager(viewpager: ViewPager) {
//        var adapter: ViewPagerAdapter? = activity?.supportFragmentManager?.let { ViewPagerAdapter(it) }
//
//        // HomeFragment is the name of Fragment and the Login
//        // is a title of tab
//        adapter?.addFragment(VendoreList(), "Vendore")
//        adapter?.addFragment(ProductsAssigned(), "Product")
//
//        // setting adapter to view pager.
//        viewpager.setAdapter(adapter)
//    }

    private fun consumeResponse(response: ApiResponse<*>) {
        Log.e(HomeFragment.TAG, "consumeResponseHome()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(HomeFragment.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is HomeViewModelResponse) {
//                                sessionStore!!.saveUserDetails(it)
                                vendorelist.addAll(it.result)
                                setVendorAdapter()



//                                requireActivity().finish()
//                                it.let {
//                                    if (StringUtils.equalsIgnoreCase(it.token?.isEmpty().toString(), "")) {
//                            //                                        ContainerActivity.gotoContainerActivity(
//                            //                                            activity!!,
//                            //                                            getString(R.string.search)
//                            //                                        )
//                                        requireActivity().finish()
//                                    } else {
//                                        utility!!.showToast(
//                                            requireActivity(),
//                                            getString(R.string.invalidUser)
//                                        )
//                                    }
//
//                                }
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        /*val apiError =
                            errorUtils!!.parseError((response.error as HttpException).response())*/
                        //Log.e(TAG, "consumeResponse() apiError - ${apiError!!.error_description}")
                        //(response.error as HttpException).response().errorBody()
                        Log.e(HomeFragment.TAG, "consumeResponse()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(HomeFragment.TAG, "consumeResponseone() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeFragment.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }

    private fun consumeResponseProduct(response: ApiResponse<*>) {
        Log.e(HomeFragment.TAG, "consumeResponseHome()" +response)
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response.let { apiResponse ->
                        apiResponse.data?.let {
                            Log.e(HomeFragment.TAG, "responsedatahome ${apiResponse.data}")
                            if (it is ProductsAssigned) {
//                                sessionStore!!.saveUserDetails(it)
                                producttoassignlist.addAll(it.result)
                                setAdapterproduct();

//                                requireActivity().finish()
//                                it.let {
//                                    if (StringUtils.equalsIgnoreCase(it.token?.isEmpty().toString(), "")) {
//                            //                                        ContainerActivity.gotoContainerActivity(
//                            //                                            activity!!,
//                            //                                            getString(R.string.search)
//                            //                                        )
//                                        requireActivity().finish()
//                                    } else {
//                                        utility!!.showToast(
//                                            requireActivity(),
//                                            getString(R.string.invalidUser)
//                                        )
//                                    }
//
//                                }
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        /*val apiError =
                            errorUtils!!.parseError((response.error as HttpException).response())*/
                        //Log.e(TAG, "consumeResponse() apiError - ${apiError!!.error_description}")
                        //(response.error as HttpException).response().errorBody()
                        Log.e(HomeFragment.TAG, "consumeResponse()data - ${response.error!!.message!!}")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e(HomeFragment.TAG, "consumeResponseone() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeFragment.TAG, "consumeResponse() Exceptionmanin - ${ex.message}")
        }

    }

    private fun setAdapterproduct() {
        try {
            productadapter= ProductListAdapter(requireContext(),this,  utility!!)
            utility!!.getRecyclerview(
                requireContext(),
                binding.productlistView,
                true,
                false
            )!!.adapter = productadapter

            //sampleJson()
            productadapter.updateList(producttoassignlist)
            productadapter.setOnItemCLickListerner(object :ProductListAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {

                    //Toast.makeText(context, wishListDetails[position].Name, Toast.LENGTH_SHORT).show()

                    val intent_assignproduct = Intent(context, AssignWorkActivity::class.java)
                    intent_assignproduct.putExtra("transid",producttoassignlist[position].id)
                    intent_assignproduct.putExtra("prodname",producttoassignlist[position].productTitle)
                    intent_assignproduct.putExtra("groupdata",vendorelist)

                    startActivity(intent_assignproduct)

                }
            } )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

//    override fun onItemClick(item: Resultdata) {
//
//        val dateString = simpleDateFormat.format(item.create_date)
//        date = dateString
//
//        val intent_assignproduct = Intent(context, EditVendorActivity::class.java)
//        intent_assignproduct.putExtra("id",item.id)
//        intent_assignproduct.putExtra("grpname",item.group_name)
//        intent_assignproduct.putExtra("createddate",date)
//
//        startActivity(intent_assignproduct)
//    }

//    private fun setAdapterproduct(any: ProductsAssigned) {
//
//        try {
//            listproduct.addAll(listOf(any))
//            productadapter= ProductListAdapter(requireContext(),  utility!!)
//            utility!!.getRecyclerview(
//                requireContext(),
//                binding.productlistView,
//                true,
//                false
//            )!!.adapter = productadapter
//
//            //sampleJson()
//            productadapter.updateList(any.result)
//            productadapter.setOnItemCLickListerner(object :ProductListAdapter.onItemClickListener{
//                override fun onItemCLick(position: Int) {
//
//                    //Toast.makeText(context, wishListDetails[position].Name, Toast.LENGTH_SHORT).show()
//
//                    val intent_assignproduct = Intent(context, AssignProductActivity::class.java)
//                    intent_assignproduct.putExtra("transid",res[position].Id)
//                    intent_assignproduct.putExtra("prodname",wishListDetails[position].ProductCategoryId)
//                    intent_assignproduct.putExtra("from_activity_name","wishlist_details")
//
//                    startActivity(intent_detail)
//
//                }
//            } )
//
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//        }
//
//    }

    override fun onItemClick(proditem: ResultProduct?) {
        val intent_transproduct = Intent(context, TransactionActivity::class.java)
        intent_transproduct.putExtra("transid", proditem?.id)
        intent_transproduct.putExtra("totqty",proditem?.quantity)
        intent_transproduct.putExtra("totamt",proditem?.totalPrice)
        intent_transproduct.putExtra("prodtitle",proditem?.productTitle)

        startActivity(intent_transproduct)
    }

//    override fun onItemClick(item: Resultdata) {
//
//        val bundle = Bundle()
//        bundle.putString("id", item.id?.toString());
//
//         Log.e(BaseActivity.TAG, "groupID - " + item.id)
//
//        ContainerActivity.gotoContainerActivity(
//            requireActivity(),
//            getString(R.string.group),
//            bundle
//
//        )
//    }

}