package fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.absyz.ssl.R
import com.absyz.ssl.databinding.FragmentVendorlistBinding

class VendoreList : BaseFragment() {

    lateinit var binding: FragmentVendorlistBinding

    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragment_vendorlist, parent, false)
        return binding.root

    }

}